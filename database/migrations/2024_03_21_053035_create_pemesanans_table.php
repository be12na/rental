<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0);
            $table->string('ordernumber');
            $table->bigInteger('nik');
            $table->string('nama');
            $table->string('alamat');
            $table->string('wa');
            $table->string('kode_marketing')->nullable();
            $table->integer('jasa_id');
            $table->integer('kendaraan_id');
            $table->integer('durasi');
            $table->date('start_at');
            $table->date('end_at');
            $table->string('driver');
            $table->integer('driver_fee');
            $table->string('file_ktp');
            $table->string('file_selfie');
            $table->string('voucher')->nullable();
            $table->integer('potongan')->default(0);
            $table->integer('biaya')->default(0);
            $table->integer('total_bayar')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pemesanans');
    }
};
