<?php

use App\Http\Controllers\AdminPemesananController;
use App\Http\Controllers\BankController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DriverfeeController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\JasaController;
use App\Http\Controllers\KategoriPemasukanController;
use App\Http\Controllers\KategoriPengeluaranController;
use App\Http\Controllers\KotaController;
use App\Http\Controllers\MobilController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\PengembalianController;
use App\Http\Controllers\RentalController;
use App\Http\Controllers\TransaksiPemasukanController;
use App\Http\Controllers\TransaksiPengeluaranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();
Route::get('/', [FrontController::class, 'pemesanan'])->name('home');

Route::get('/fetch-durasi', [RentalController::class, 'durasiFetch'])->name('durasiFetch');

Route::get('/service/rental', [RentalController::class, 'mobil'])->name('rental.mobil');
Route::get('/service/rental/pemesanan', [RentalController::class, 'pemesanan'])->name('rental.pemesanan');
Route::post('/service/rental/pemesanan/store', [FrontController::class, 'storePemesanan'])->name('rental.pemesanan.store');
Route::get('//service/rental/pemesanan/detail', [FrontController::class, 'detailPemesanan'])->name('rental.pemesanan.detail');


Route::get('/fganti_psswrd', [HomeController::class, 'fganti_psswrd'])->name('fganti_psswrd');
Route::put('/update_psswrd/{user}', [HomeController::class, 'update_psswrd'])->name('update_psswrd');

Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', [HomeController::class, 'home'])->name('home');

    Route::get('/profile', [HomeController::class, 'profile'])->name('profile');

    Route::get('/laporan/', [ReportController::class, 'index'])->name('laporan.index');

    Route::get('/pemesanan/', [AdminPemesananController::class, 'index'])->name('pemesanan.index');
    Route::get('/pemesanan/{id}/detail', [AdminPemesananController::class, 'detail'])->name('pemesanan.detail');

    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    // Route::resource('menu', MenuController::class);
    Route::resource('customer', CustomerController::class);
    Route::resource('inventory', InventoryController::class);
    Route::resource('order', OrderController::class);
    Route::resource('finishorder', PengembalianController::class);
    Route::resource('invoice', InvoiceController::class);

    Route::resource('kategori/pemasukan', KategoriPemasukanController::class)->names('kategori.pemasukan');
    Route::resource('kategori/pengeluaran', KategoriPengeluaranController::class)->names('kategori.pengeluaran');

    Route::resource('transaksi/pemasukan', TransaksiPemasukanController::class)->names('transaksi.pemasukan');
    Route::resource('transaksi/pengeluaran', TransaksiPengeluaranController::class)->names('transaksi.pengeluaran');

    Route::resource('mobil',MobilController::class);
    Route::resource('kota',KotaController::class);
    Route::resource('jasa',JasaController::class);
    Route::resource('bank',BankController::class);
    Route::resource('driverfee',DriverfeeController::class);


    Route::get('/getphoto', [InventoryController::class, 'getphoto']);
    Route::get('/getcustomer', [CustomerController::class, 'getcustomer']);
    Route::get('/getinventory', [InventoryController::class, 'getinventory']);
    Route::get('/cetakinv/{no_inv}', [InvoiceController::class, 'cetak']);

    Route::get('/pengembalian/{order_id}', [OrderController::class, 'pengembalian']);
    Route::get('/invoicing/{id}', [PengembalianController::class, 'invoicing'])->name('report.invoicing');
});
