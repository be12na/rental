<div>
    <div class="card"> 
        <div class="card-body px-4 py-4-5">
            <div class="row">
                <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-3 d-flex justify-content-start ">
                    <i class="bi bi-arrow-up-short red stats-icon text-white h1"></i>
                    {{-- <div class="stats-icon blue mb-2">
                       
                    </div> --}}
                </div>
                <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-9">
                    <h6 class="text-muted font-semibold">Total Pengeluaran</h6>
                    <h6 class="font-extrabold mb-0">Rp{{number_format($total,0,'','.')}}</h6>
                </div>
            </div>
        </div>
    </div>
</div>