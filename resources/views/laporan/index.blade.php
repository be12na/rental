@extends('shared_pages.layout')
@section('content')
<section class="section">
  <div class="card">
    <div class="card-header text-end">
      <h4>{{ $dt['title'] }}</h4>
    </div>
    <div class="card-body">
      
     
      <div class="mb-5">

        <form action="" method="get">
        <div class="row align-items-center">

            <div class="col-lg-2">
              <div class="form-group">
                  <label class="mt-2">Total</label>
                  <h4 class="fw-bold">Rp{{number_format($dataTotalSaldo,0,'','.')}}</h4>
              </div>
          </div>
          
            <div class="col-lg-2">
              <div class="form-group">
                  <label class="mt-2">Jenis</label>
                  <select class="form-select @error('kategori') is-invalid @enderror" id="kategori" name="jenis" >
                      <option value="">Choose...</option>
                      <option {{Request('jenis') == "pemasukan" ? "selected" : ""}} value="pemasukan">Pemasukan</option>
                      <option {{Request('jenis') == "pengeluaran" ? "selected" : ""}} value="pengeluaran">Pengeluaran</option>
                  </select>
                  @error('order')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                  @enderror
              </div>
            </div>

            <div class="col-lg-2">
              <div class="form-group">
                  <label class="mt-2">Tgl Awal</label>
                  <input type="date" class=" form-control @error('jasa') is-invalid @enderror" name="awal" value="{{ Request('awal') }}">
                  @error('jasa')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                  @enderror
              </div>
           </div>

          <div class="col-lg-2">
            <div class="form-group">
                <label class="mt-2">Tgl Akhir</label>
                <input type="date" class=" form-control @error('jasa') is-invalid @enderror" name="akhir" value="{{ Request('akhir') }}">
                @error('jasa')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
          </div>

          <div class="col-lg-4 mt-3">
            <div class="">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{route('laporan.index')}}" class="btn btn-warning">Reset Filter</a>
            </div>
          </div>
        
        </div>
        </form>
        
      </div>

      <div class="table-responsive">
        <table class="table table-bordered" id="table1">  <thead>
            <tr class="bg-orange text-light">
              <th>No</th>
              <th>Tanggal</th>
              <th>Janis</th>
              <th>Transaksi</th>
              <th>Saldo</th>
            </tr>
          </thead>
          <tbody>
            <?php $k = 0; ?>
            @forelse ($dataList as $p)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ date('d-M-y', strtotime($p->tanggal)) }}</td>
               
                <td>
                  <?php
                  if ($p->type == 'pengeluaran') {
                    $color = 'bg-danger';
                  } else {
                    $color = 'bg-success';
                  }
                  ?>
                  <div class="badges">
                    <span class="badge {{ $color }}">
                      {{ $p->type }}
                    </span>
                  </div>
                </td>

                <td>{{ $p->kategori->nama }}</td>
               
                <td>{{ number_format($p->total, 0, ',', '.') }}</td>
                
              </tr>
              <?php $k++; ?>
            @empty
              <tr>
                <td colspan="7" class="text-danger text-center">Data order rental belum ada</td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@endSection
