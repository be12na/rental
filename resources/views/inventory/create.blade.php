@extends('shared_pages.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('inventory.store') }}" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama Inventory</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-text">
                                        <input class="form-check-input" type="checkbox" value=""
                                            aria-label="Checkbox for following text input">
                                    </div>
                                    <input type="text" name="nama"
                                        class="form-control @error('nama') is-invalid @enderror" placeholder="Nama"
                                        autofocus>
                                    @error('nama')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Quantity</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-text">
                                        <input class="form-check-input" type="checkbox" value=""
                                            aria-label="Checkbox for following text input">
                                    </div>
                                    <input type="text" name="qtt"
                                        class="form-control @error('qtt') is-invalid @enderror"
                                        placeholder="Quantity Inventory">
                                    @error('qtt')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Harga Sewa / Hari</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="input-group mb-3">
                                <div class="input-group-text">
                                    <input class="form-check-input" type="checkbox" value=""
                                        aria-label="Checkbox for following text input">
                                </div>
                                <input type="number" name="harga_sewa"
                                    class="form-control @error('harga_sewa') is-invalid @enderror"
                                    placeholder="Harga sewa per hari">
                                @error('harga_sewa')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Keterangan</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea class="form-control @error('keterangan') is-invalid @enderror" style="height:125px" name="keterangan"
                                    placeholder="Keterangan produk"></textarea>
                                @error('keterangan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Photo Inventory</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="input-group mb-3">
                                <input type="file" name="photo"
                                    class="form-control @error('photo') is-invalid @enderror">
                                @error('photo')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between">
                        <div class="col">
                            <a href="{{ route('inventory.index') }}" type="button" class="btn btn-secondary">Kembali</a>
                        </div>
                        <div class="col text-end">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
