@extends('shared_pages.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div>
        <div class="card-body">
            <form class="forms-sample" action="{{ route('customer.store') }}" method="POST">
                @csrf
                @method('post')

                <div class="row form-group">
                    <div class="col-sm-12">
                        <label>NIK</label>
                        <div class="input-group mb-3">
                            
                            <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik"
                                value="{{ old('nik') }}" placeholder="NIK customer" autofocus>
                            @error('nik')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
               
                <div class="row form-group">
                    <div class="col-sm-12">
                        <label>Nama Customer</label>
                        <div class="input-group mb-3">
                            
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                value="{{ old('nama') }}" placeholder="Nama customer" autofocus>
                            @error('nama')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12">
                        <label>Telp</label>
                        <div class="input-group mb-3">
                            
                            <input type="text" class="form-control @error('telp') is-invalid @enderror" name="telp"
                                value="{{ old('telp') }}">
                            @error('telp')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12">
                        <label>Kota</label>
                        <div class="input-group mb-3">
                            
                            <select type="text" class="form-control @error('kota') is-invalid @enderror"
                                name="kota" value="{{ old('kota') }}">
                                <option value="">-- Pilih Kota --</option>
                                @foreach ($kotaList as $p)
                                    <option value="{{ $p->id }}">{{ $p->kota }}</option>
                                @endforeach
                            </select>
                            @error('kota')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-sm-12">
                        <label>Alamat Customer</label>
                        <div class="input-group mb-3">
                            
                            <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                                value="{{ old('alamat') }}" id="formFile">
                            @error('alamat')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
               
                <div class="row justify-content-between">
                    <div class="col">
                        <a href="{{ route('customer.index') }}" class="btn btn-secondary">Kembali</a>
                    </div>
                    <div class="col text-end">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
