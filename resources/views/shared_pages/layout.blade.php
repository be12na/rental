@include('shared_pages.header')

<body>
    <div id="app">
        {{-- menu sidebar --}}
        @include('shared_pages.sidebar')
        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>

            <div class="page-heading">
                {{-- heading halaman --}}
                @include('shared_pages.heading')

                <!-- awal halaman content -->
                @yield('content')
                <!-- akhir halaman content -->
            </div>

            {{-- footer --}}
            @include('shared_pages.footer')
        </div>
    </div>
    <script src="/assets/js/bootstrap.js"></script>
    <script src="/assets/js/app.js"></script>

    <script src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
    <script src="/assets/js/pages/datatables.js"></script>

</body>

</html>
