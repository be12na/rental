<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aunaja Corporation</title>

    <link rel="stylesheet" href="/assets/css/main/app.css">
    <link rel="stylesheet" href="/assets/css/main/app-dark.css">
    <link rel="shortcut icon" href="/assets/images/logo/favicon.svg" type="image/x-icon">
    <link rel="shortcut icon" href="/assets/images/logo/favicon.png" type="image/png">

    <link rel="stylesheet" href="/assets/css/pages/fontawesome.css">
    <link rel="stylesheet" href="/assets/extensions/datatables.net-bs5/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="/assets/css/pages/datatables.css">
    <link rel="stylesheet" href="/assets/vendors/sweetalert2/sweetalert2.css">

  
    <style>
        .bg-darkblue {
            background-color: darkblue;
        }

        .bg-orange {
            background-color: orangered;
        }

        .text-orange {
            color: orangered;
        }
    </style>
    <script src="/assets/extensions/jquery/jquery.min.js"></script>
    <script src="/assets/vendors/sweetalert2/sweetalert2.min.js"></script>
    <script src="{{asset('assets/js/clipboard.min.js')}}"></script>

</head>
