@extends('shared_pages.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('kota.store') }}" name="myForm" method="POST" onsubmit="return validateForm()">
                @csrf
                @method('POST')
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="d-sm-flex">
                        <a href="{{ route('mobil.index') }}" class="btn btn-md btn-circle btn-outline-danger">
                            <i class="bi bi-skip-backward-fill"></i> Back
                        </a>
                        &nbsp;
                    </div>
                    {{-- <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                        Submit Order <i class="bi bi-sd-card-fill"></i>
                    </button> --}}
                </div>

                <!-- content -->
                <div class="row">
             
                    <div class="col-lg-12">
                        <div class="card border-bottom-primary shadow mb-4">
                            <div class="card-header py-3 bg-orange">
                                <h6 class="m-0 font-weight-bold text-white">Data Kota</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="mt-2">Nama Kota</label>
                                            <input type="text" class=" form-control @error('kota') is-invalid @enderror" name="kota" value="{{ old('kota') }}">
                                            @error('kota')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                  
                                  
                                </div>

                                <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                                    Submit Kota <i class="bi bi-sd-card-fill"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </form>
        </div>
    </div>
    <script>
       
    </script>
@endsection
