@extends('shared_pages.layout')
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header text-end">
                <h4>{{ $dt['title'] }}</h4>
            </div>
            <div class="card-body">
                <div class="mb-5"></div>
               
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <tr class="bg-orange text-light">
                                <th>No</th>
                                <th>Mulai</th>
                                <th>Berakhir</th>
                                <th>Durasi</th>
                                <th>Penyewa</th>
                                <th>No. Whatsapp</th>
                                <th>total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $k = 0; ?>
                            @forelse ($dataList as $p)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                   
                                   
                                    <td>{{ $p->start_at }}</td>
                                    <td>{{ $p->end_at }}</td>
                                    <td>{{ $p->durasi }}</td>
                                    <td>{{ $p->nama }}</td>
                                    <td>{{ $p->wa }}</td>
                                    <td>{{ number_format($p->total_bayar,0,'','.') }}</td>
                                   
                                   
                                    
                                    <td>

                                        <div class="d-flex">

                                            <a class="btn btn-sm btn-primary me-1" href="{{ route('pemesanan.detail', $p->ordernumber) }}"
                                                title="Edit Customer"><i class="bi bi-pencil"></i></a>
                                           
                                           

                                        </div>
                                        
                                    </td>
                                </tr>
                                <?php $k++; ?>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-danger text-center">Data order rental belum ada</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script>
       
    </script>
@endSection
