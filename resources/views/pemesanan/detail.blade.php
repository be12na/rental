@extends('shared_pages.layout')
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header text-end">
                <h4>{{ $dt['title'] }}</h4>
            </div>
            <div class="card-body">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="d-sm-flex">
                        <a href="{{ route('pemesanan.index') }}" class="btn btn-md btn-circle btn-outline-danger">
                            <i class="bi bi-skip-backward-fill"></i> Back
                        </a>
                        &nbsp;
                    </div>
                  
                </div>

                <div class="row g-5">
               

                    <div class="col-md-12">
    
                      <h4 class="mb-3">Detail Pemesanan</h4>
                        
                        
                        <div class="row">
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Tanggal Mulai Rental</p>
                            <p class="fw-bold">{{$order->start_at}}</p>
                          </div>
    
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Tanggal Selesai Rental</p>
                            <p class="fw-bold">{{$order->end_at}}</p>
                          </div>
    
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Kendaraan</p>
                            <p class="fw-bold">{{$order->mobil->merek}} {{$order->mobil->tipe}} {{$order->mobil->warna}}</p>
                          </div>
    
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Tanggal Durasi Rental</p>
                            <p class="fw-bold">{{$order->durasi}} Jam</p>
                          </div>
    
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Driver</p>
                            <p class="fw-bold">
                                @if ($order->driver == 0)
                                    Tanpa Driver
                                @else
                                  {{$order->driverfee->judul}} 
                                @endif
                             
                            </p>
                          </div>
    
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Driver Durasi</p>
                            <p class="fw-bold">
                              @if ($order->driver == 0)
                                  0
                              @else
                                {{$order->driverfee->durasi}} 
                              @endif
                             
                            </p>
                          </div>
                        </div>
    
                        <hr class="my-4">
                      
    
                        <h4 class="mb-3">Data Pemesan</h4>
    
                        <div class="row">
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">NIK</p>
                            <p class="fw-bold">{{$order->nik}}</p>
                          </div>
    
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Nama</p>
                            <p class="fw-bold">{{$order->nama}}</p>
                          </div>
    
                          <div class="col-md-6">
                            <p class="m-0 text-muted text-sm">Nomor Whatsapp</p>
                            <p class="fw-bold">{{$order->wa}}</p>
                          </div>
    
                          <div class="col-md-12">
                            <p class="m-0 text-muted text-sm">Alamat Saat ini</p>
                            <p class="fw-bold">{{$order->alamat}}</p>
                          </div>
                        </div>
    
                      
                        
              
                       
    
                       
                        <hr class="my-4">
    
                        <h4 class="mb-3">Dokumen</h4>
    
                        <div class="row">
                          <div class="col-md-2">
                            <a href="{{asset('storage/'.$order->file_ktp)}}">
                              <img src="{{asset('storage/'.$order->file_ktp)}}" alt="" class="w-100">
                            </a>
                          </div>
                          <div class="col-md-2">
                            <a href="{{asset('storage/'.$order->file_selfie)}}">
                              <img src="{{asset('storage/'.$order->file_selfie)}}" alt="" class="w-100">
                            </a>
                          </div>
                        </div>
              
                       
                      
                    </div>

                    <hr class="my-4">

                    <div class="col-md-12">
                        
    
                        <h4 class="mb-3">Rincian Pembayaran</h4>
    
                        <div class="d-flex justify-content-between mb-3" id="rincian">
    
                          <div>
                            <h6 class="my-0">{{$order->mobil->merek}} {{$order->mobil->tipe}} {{$order->mobil->warna}}</h6>
                            <small class="text-body-secondary">{{$order->durasi}} jam</small>
                          </div>
                          <span class="text-body-secondary">Rp{{number_format($order->biaya,0,'','.')}}</span>
      
                        </div>
    
                        <div class="d-flex justify-content-between mb-3" id="rincian">
    
                          <div>
                            <h6 class="my-0">
                              
                              @if ($order->driver == 0)
                                  Tanpa Driver
                              @else
                                Driver {{$order->driverfee->judul}}
                              @endif
                             </h6>
                            <small class="text-body-secondary">
                              @if ($order->driver == 0)
                                0
                              @else
                                {{$order->driverfee->durasi}}
                              @endif
                             
                            </small>
                          </div>
                          <span class="text-body-secondary">Rp{{number_format($order->driver_fee,0,'','.')}}</span>
      
                        </div>
    
    
                        {{-- <div class="d-flex justify-content-between mb-3" id="rincian">
    
                          <div class="text-success">
                            <h6 class="my-0">Promo code</h6>
                            <small>EXAMPLECODE</small>
                          </div>
                          <span class="text-success">−$5</span>
      
                        </div> --}}
    
                        <div class="d-flex justify-content-between mb-3">
    
                          <span>Total</span>
                          <strong>Rp{{number_format($order->total_bayar,0,'','.')}}</strong>
      
                        </div>
              
                       
                      
                    </div>
    
                   
                </div>
            </div>
        </div>
    </section>
    <script>
       
    </script>
@endSection
