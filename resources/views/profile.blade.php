@extends('shared_pages.layout')
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header text-end">
                <h4>{{ $dt['title'] }}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-warning card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="rounded-circle img-fluid" width="150px" src="/assets/images/faces/5.jpg"
                                        alt="User profile picture">
                                </div>
                                <h3 class="text-center mt-3">{{ Auth::user()->name }}</h3>
                                <p class="text-muted text-center">Software Engineer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card card-warning card-outline">
                            <div class="card-header p-2">
                                <h5 class="text-center">Data Profile</h5>
                            </div>
                            <div class="card-bdy p-2">
                                <form class="form-horizontal mt-3">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Nama</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" value="{{ Auth::user()->name }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" value="{{ Auth::user()->email }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Pengalaman</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" value="Software Engineer 5 Tahun" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Skills</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="Back End Developer" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Hoby</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="Bersepeda" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Topic Kesukaan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="Sains & Teknologi" readonly>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
