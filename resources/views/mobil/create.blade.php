@extends('shared_pages.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('mobil.store') }}" name="myForm" method="POST" onsubmit="return validateForm()">
                @csrf
                @method('POST')
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="d-sm-flex">
                        <a href="{{ route('mobil.index') }}" class="btn btn-md btn-circle btn-outline-danger">
                            <i class="bi bi-skip-backward-fill"></i> Back
                        </a>
                        &nbsp;
                    </div>
                    {{-- <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                        Submit Order <i class="bi bi-sd-card-fill"></i>
                    </button> --}}
                </div>

                <!-- content -->
                <div class="row">
             
                    <div class="col-lg-12">
                        <div class="card border-bottom-primary shadow mb-4">
                            <div class="card-header py-3 bg-orange">
                                <h6 class="m-0 font-weight-bold text-white">Data Kendaraan</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Nomor Polisi</label>
                                            <input type="text" class="form-control" name="nopol" value="{{ old('nopol') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Merek</label>
                                            <input type="text" class=" form-control @error('merek') is-invalid @enderror" name="merek" value="{{ old('merek') }}">
                                            @error('merek')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Tipe</label>
                                            <input type="text" class=" form-control @error('tipe') is-invalid @enderror" name="tipe" value="{{ old('tipe') }}">
                                            @error('tipe')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Warna</label>
                                            <input type="text" class=" form-control @error('warna') is-invalid @enderror" name="warna" value="{{ old('warna') }}">
                                            @error('warna')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="mt-2">Harga 6 jam</label>
                                            <input type="text" class=" form-control @error('harga_6') is-invalid @enderror" name="harga_6" value="{{ old('harga_6') }}">
                                            @error('harga_6')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="mt-2">Harga 12 jam</label>
                                            <input type="text" class=" form-control @error('harga_12') is-invalid @enderror" name="harga_12" value="{{ old('harga_12') }}">
                                            @error('harga_12')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="mt-2">Harga 24 jam</label>
                                            <input type="text" class=" form-control @error('harga_24') is-invalid @enderror" name="harga_24" value="{{ old('harga_24') }}">
                                            @error('harga_24')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="mt-2">Harga Perday</label>
                                            <input type="text" class=" form-control @error('harga_day') is-invalid @enderror" name="harga_day" value="{{ old('harga_day') }}">
                                            @error('harga_day')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                  
                                </div>

                                <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                                    Submit Kendaraan <i class="bi bi-sd-card-fill"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </form>
        </div>
    </div>
    <script>
       
    </script>
@endsection
