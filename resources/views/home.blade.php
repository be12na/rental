@extends('shared_pages.layout')
@section('content')
    <section class="section">

        <div class="row">
            <div class="col-6 col-lg-4 col-md-6">
                <x:stats.pemasukan />
            </div>
            
            <div class="col-6 col-lg-4 col-md-6">
                <x:stats.pengeluaran />
            </div>
            
            <div class="col-6 col-lg-4 col-md-6">
                <x:stats.saldo />
            </div>
            <div class="col-6 col-lg-4 col-md-6">
                <x:stats.customer />
            </div>

            <div class="col-6 col-lg-4 col-md-6">
                <x:stats.mobil />
            </div>
        </div>

        <div class="card">
            {{-- <div class="card-header text-end">
                <h4>{{ $dt['title'] }}</h4>
            </div> --}}
            <div class="card-body">
                <h4>Selamat Datang</h4>
            </div>
        </div>
    </section>
@endsection
