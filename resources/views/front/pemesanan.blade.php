@extends('shared_pages.order-layout')
@section('content')
    <div class="card">
        {{-- <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div> --}}
        <div class="card-body">
            <div class="py-5 text-center">
                {{-- <img class="d-block mx-auto mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> --}}
                <h2>Form Pemesanan</h2>
                <p class="lead">Silahkan Isi Form Pemesanan dibawah ini dengan Lengkap dan Benar.</p>
              </div>

              <form  action="{{route('rental.pemesanan.store')}}" method="post" enctype="multipart/form-data">
                @csrf
          
              <div class="row g-5">
               
                <div class="col-md-7 col-lg-8">
                  
                 
                  
                    <h4 class="mb-3">Detail Pemesanan</h4>
                    
                        <div class="row g-3">

                          <div class="col-md-12">
                            <label for="kendaraan" class="form-label">Pilih Kendaraan</label>
                            <select class="form-select @error('kendaraan') is-invalid @enderror" id="kendaraan" name="kendaraan" >
                                <option value="">Choose...</option>
                                @foreach ($mobilList as $item)
                                    <option {{old('kendaraan') == $item->id ? 'selected' : ''}} value="{{$item->id}},{{$item->merek}} {{$item->tipe}}">{{$item->merek}} {{$item->tipe}}</option>
                                @endforeach
                            
                            </select>
                            @error('kendaraan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label for="mulai" class="form-label">Tanggal Mulai Rental</label>
                            <input type="date" class="form-control @error('mulai') is-invalid @enderror" id="mulai" name="mulai" min="{{date("Y-m-d")}}" placeholder="" value="{{old('mulai')}}" >
                            @error('mulai')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label for="selesai" class="form-label">Tanggal Selesai Rental</label>
                            <input type="date" class="form-control @error('selesai') is-invalid @enderror" id="selesai" name="selesai" min="{{date("Y-m-d")}}" placeholder="" value="{{old('selesai')}}" >
                            @error('selesai')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="col-md-6">
                          <label for="durasi" class="form-label">Durasi</label>
                          <select class="form-select @error('durasi') is-invalid @enderror" id="durasi" name="durasi" >
                            <option value="">Choose...</option>
                            {{-- <option {{old('durasi') == '6' ? 'selected' : ''}} value="6">6 Jam</option>
                            <option {{old('durasi') == '12' ? 'selected' : ''}} value="12">12 Jam</option>
                            <option {{old('durasi') == '24' ? 'selected' : ''}} value="24">24 Jam</option> --}}
                          </select>
                          @error('durasi')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                          @enderror
                        </div>

                       

                        

                        <div class="col-md-6">
                          <label for="driver" class="form-label">Driver</label>
                          <select class="form-select @error('driver') is-invalid @enderror" id="driver" name="driver" >
                            <option value="">Choose...</option>
                            <option value="0,Tanpa Driver,0,0">Tanpa Driver</option>
                            @foreach ($driverList as $item)
                            <option value="{{$item->id}},{{$item->judul}},{{$item->durasi}},{{$item->harga}}">{{$item->judul}} {{$item->durasi}} Rp{{$item->harga}}</option>
                            @endforeach
                          </select>
                          @error('driver')
                          <div class="text-danger mt-1" style="font-size: .875em;">
                              {{ $message }}
                          </div>
                          @enderror
                          
                      </div>

                    </div>

                    <hr class="my-4">

                    <h4 class="mb-3">Data Pemesan</h4>

                    
                    <div class="my-3">
                     
                      <div class="row g-3 mb-3">
                        <div class="col-md-12">
                          <label for="nik" class="form-label">NIK KTP</label>
                          <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" placeholder="" value="{{old('nik')}}">
                          @error('nik')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                          @enderror
                        </div>

                        <div class="col-md-6">
                          <label for="nama" class="form-label">Nama (Sesuai Identitas)</label>
                          <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="" value="{{old('nama')}}"  >
                        
                          @error('nama')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                          @enderror
                          
                        </div>

                        <div class="col-md-6">
                          <label for="wa" class="form-label">Nomor Whatsapp</label>
                          <input type="text" class="form-control @error('wa') is-invalid @enderror" id="wa" name="wa"  placeholder="" value="{{old('wa')}}" >
                          @error('wa')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                          @enderror
                        </div>

                        
                        <div class="col-md-12">
                          <label for="alamat" class="form-label">Alamat saat ini</label>
                          <textarea class="form-control @error('alamat') is-invalid @enderror" id="alamat" rows="3" name="alamat">{{old('alamat')}}</textarea>
                          @error('alamat')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                          @enderror
                        </div>
                      </div>
                     
                      

                     

                     
                    </div>

                   
          

       

                    <hr class="my-4">
          
                    <h4 class="mb-3">Upload Dokumen</h4>
          
                    <div class="mb-3">
                      <label for="formFile" class="form-label">Identitas KTP/SIM</label>
                      <input class="form-control @error('file_ktp') is-invalid @enderror" type="file" name="file_ktp" id="formFile">
                      @error('file_ktp')
                        <div class="text-danger mt-1" style="font-size: .875em;">
                            {{ $message }}
                        </div>
                      @enderror
                    </div>
                    
                    <div class="mb-3">
                      <label for="formFile" class="form-label">Identitas + Selfie</label>
                      <input class="form-control @error('file_selfie') is-invalid @enderror" type="file" name="file_selfie" id="formFile">
                      @error('file_selfie')
                        <div class="text-danger mt-1" style="font-size: .875em;">
                            {{ $message }}
                        </div>
                      @enderror
                    </div>

                    <div class="mt-3">
                      <div class="form-check">
                        <input class="form-check-input @error('agree')  border border-danger border-2 @enderror" type="checkbox" name="agree" value="1" id="flexCheckChecked" >
                        <label class="form-check-label @error('agree') text-danger @enderror" for="flexCheckChecked">
                          Dengan ini saya Menyetujui Syarat dan Ketentuan Sistem Rental
                        </label>
                      </div>
                      @error('agree')
                      <div class="text-danger mt-1" style="font-size: .875em;">
                          Kamu harus setujui ketentuan kami
                      </div>
                      @enderror
                    </div>
                     
                  
          
                   
          
                   
                    <hr class="my-4">
          
                    <button class="w-100 btn btn-primary btn-lg d-md-block d-none" type="submit">Buat Pemesanan</button>
                  
                </div>

                <div class="col-md-5 col-lg-4 order-md-last">
                  <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-primary">Rincian Pemesanan</span>
                    {{-- <span class="badge bg-primary rounded-pill">3</span> --}}
                  </h4>

                  <div class="my-3">

                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Promo code">
                      <button type="submit" class="btn btn-secondary">Redeem</button>
                    </div>
                  
                  </div>

                  <div class="mb-3">
                    <label for="kode" class="form-label">Kode Marketing (Kosongkan Jika Tidak Ada)</label>
                    <input type="text" class="form-control" id="kode" name="kode" placeholder="" value="{{old('kode')}}" >
                  
                      
                   
                  </div>

                  <hr class="my-4">

                  <div class="d-flex justify-content-between mb-3 d-none" id="rental">

                    <div>
                      <h6 class="my-0" id="mobilName"></h6>
                      <small class="text-body-secondary"><span id="rentalDurasi"></span> jam</small>
                    </div>
                    <span class="text-body-secondary">Rp<span id="rentalBiaya"></span></span>

                  </div>

                  <div class="d-flex justify-content-between mb-3 d-none" id="driverWrap">

                    <div>
                      <h6 class="my-0" id="driverJudul"></h6>
                      <small class="text-body-secondary"><span id="driverDurasi"></span></small>
                    </div>
                    <span class="text-body-secondary">Rp<span id="hargaDriver"></span></span>

                  </div>

                  {{-- <div class="d-flex justify-content-between">

                    <div class="text-success">
                      <h6 class="my-0">Promo code</h6>
                      <small>EXAMPLECODE</small>
                    </div>
                    <span class="text-success">−$5</span>

                  </div> --}}

                  <div class="d-flex justify-content-between mb-3">

                    <span>Total</span>
                    <strong>Rp<span id="totalBayar">0</span></strong>

                  </div>


                  {{-- <ul class="list-group mb-3">
                    <li id="rincian" class="list-group-item d-flex justify-content-between lh-sm d-none">
                      <div>
                        <h6 class="my-0" id="jasaName"></h6>
                        <small class="text-body-secondary"><span id="jasaDurasi"></span> jam</small>
                      </div>
                      <span class="text-body-secondary">Rp<span id="totalBiaya"></span></span>
                    </li>
                   
                    <li class="list-group-item d-flex justify-content-between lh-sm">
                      <div class="text-success">
                        <h6 class="my-0">Promo code</h6>
                        <small>EXAMPLECODE</small>
                      </div>
                      <span class="text-success">−$5</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between">
                      <span>Total</span>
                      <strong>Rp<span id="totalBayar">0</span></strong>
                    </li>
                  </ul> --}}

                 
          
                  
                </div>
              </div>

              <div class="d-block d-md-none">

                <hr class="my-4 ">

                <button class="w-100 btn btn-primary btn-lg mt-3" type="submit">Buat Pemesanan</button>


              </div>

              
              </form> 
        </div>
    </div>
    <script>

        var durasi = 0;
        var kendaraan;
        var hargaRental = 0;
        var rentalDurasi
       

        var driverJudul;
        var driverDurasi;
        var hargaDriver = 0;

        $('#kendaraan').on('change', function () {

          var idkendaraan = this.value;

          const myArray = idkendaraan.split(",");

         

          $("#durasi").html('<option value="">loading...</option>');
          $.ajax({
              url: "{{route('durasiFetch')}}",
              type: "GET",
              data: {
                  kendaraan_id:myArray[0],
                  _token: '{{csrf_token()}}'
              },
              dataType: 'json',
              success: function (res) {

                  console.log(res);

              
                  $('#durasi').html('<option value="">Pilih Durasi</option>');

                  $.each(res, function (key, value) {

                      $("#durasi").append('<option value="'  + value.waktu + ','  + value.biaya + '">' + value.waktu +'Jam Rp '+ value.biaya.toLocaleString('id-ID')+  '</option>');
                  });
              }
          });
        });

        $('#durasi').on('change', function () {
            var durasi = this.value;

            var kendaraan = $('#kendaraan').find(":selected").val();
 

            const myArray = durasi.split(",");
            const kendaraanArray = kendaraan.split(",");

            hargaRental = parseInt(myArray[1]);
            rentalDurasi = myArray[0];
            mobilnama = kendaraanArray[1];

            // console.log(myArray[0]);
            // console.log("ini"+myArray[1]);

            // ongkir = parseInt(myArray[1]);

            // document.getElementById("ongkir").classList.remove("d-none");

            // document.getElementById("jumlahOngkir").innerHTML = parseInt(myArray[1]).toLocaleString('id-ID');

            // document.getElementById("kurirPaket").innerHTML = getKurir+' '+myArray[0];

            // payment()

            document.getElementById("rental").classList.remove("d-none");
            document.getElementById("mobilName").innerHTML = mobilnama;
            document.getElementById("rentalBiaya").innerHTML = hargaRental;
            document.getElementById("rentalDurasi").innerHTML = rentalDurasi;

            getRincian();
  
        });

        $('#driver').on('change', function () {
            var driver = this.value;

            const myArray = driver.split(",");

            console.log(driver);
          
            driverJudul = myArray[1];
            driverDurasi = myArray[2];
            hargaDriver = parseInt(myArray[3]);

            document.getElementById("driverWrap").classList.remove("d-none");
            document.getElementById("driverJudul").innerHTML = driverJudul;
            document.getElementById("driverDurasi").innerHTML = driverDurasi;
            document.getElementById("hargaDriver").innerHTML = hargaDriver;
          
            getRincian();
  
        });

        function getRincian(){
            // console.log('durasi '+durasi);
            // console.log('biaya '+biaya);
            // console.log('jasa '+jasaName);

            var tb = hargaRental+hargaDriver;

            
            document.getElementById("totalBayar").innerHTML = tb;
        }
       
    </script>
@endsection
