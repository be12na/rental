@extends('shared_pages.order-layout')
@section('content')
    <div class="card">
        {{-- <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div> --}}

        @if ($order)
            
        
        <div class="card-body">
            <div class="py-5 text-center">
                {{-- <img class="d-block mx-auto mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> --}}
                <h2>Pesanan Anda Sudah diterima.</h2>
                
              </div>


              
          
              <div class="row g-5">
               
                <div class="col-md-6">
                    <p class="lead">Silahkan Transfer Ke Rekening dibawah ini:</p>
            
                    <div class="row">

                      @foreach ($bankList as $item)

                      <div class="col-md-6">
                                          
                        <div class="card border border-secondary mb-3 " style="max-width: 18rem;">
                      
                          <div class="card-body text-secondary">
                            <button class="btn btn-sm btn-outline-primary btn-copy mb-3" data-clipboard-text="{{$item->norek_bank}}" aria-label="{{$item->id}}">
                              <div class="d-flex align-items-center">

                                {{-- <i class="bi bi-clipboard-fill text-sm"> --}}

                                <span id="icon-{{$item->id}}" class="mb-1 "><i class="bi bi-clipboard-fill text-sm"></i></span>
                                <span id="copy-text-{{$item->id}}" class=" ms-2">Copy NoRek</span>
                              </div>
                             
                            </button>
                            <h5 class="card-title">{{$item->nama_bank}}</h5>
                            <p class="card-text m-0">A/N J{{$item->akun_bank}}</p>
                            <p class="card-text fw-bold">{{$item->norek_bank}}</p>
                          </div>
                        </div>

                      </div>
                          
                      @endforeach

                    </div>

                    <p>
                      Atau bisa Pembayaran COD <br>
                      Silahkan Hubungi Admin jika ada kendala atau ada yang ingin ditanyakan
                    </p>

                    <a href="http://wa.me/6287889808787" class="btn btn-primary">Hubungi Admin</a>

                   
                    <hr class="my-4">

                    <h4 class="mb-3">Rincian Pembayaran</h4>

                    <div class="d-flex justify-content-between mb-3" id="rincian">

                      <div>
                        <h6 class="my-0">{{$order->mobil->merek}} {{$order->mobil->tipe}} {{$order->mobil->warna}}</h6>
                        <small class="text-body-secondary">{{$order->durasi}} jam</small>
                      </div>
                      <span class="text-body-secondary">Rp{{number_format($order->biaya,0,'','.')}}</span>
  
                    </div>

                    <div class="d-flex justify-content-between mb-3" id="rincian">

                      <div>
                        <h6 class="my-0">
                          
                          @if ($order->driver == 0)
                              Tanpa Driver
                          @else
                            Driver {{$order->driverfee->judul}}
                          @endif
                         </h6>
                        <small class="text-body-secondary">
                          @if ($order->driver == 0)
                            0
                          @else
                            {{$order->driverfee->durasi}}
                          @endif
                         
                        </small>
                      </div>
                      <span class="text-body-secondary">Rp{{number_format($order->driver_fee,0,'','.')}}</span>
  
                    </div>


                    {{-- <div class="d-flex justify-content-between mb-3" id="rincian">

                      <div class="text-success">
                        <h6 class="my-0">Promo code</h6>
                        <small>EXAMPLECODE</small>
                      </div>
                      <span class="text-success">−$5</span>
  
                    </div> --}}

                    <div class="d-flex justify-content-between mb-3">

                      <span>Total</span>
                      <strong>Rp{{number_format($order->total_bayar,0,'','.')}}</strong>
  
                    </div>
          
                   
                  
                </div>

                <div class="col-md-6">

                  <h4 class="mb-3">Detail Pemesanan</h4>
                    
                    
                    <div class="row">
                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Tanggal Mulai Rental</p>
                        <p class="fw-bold">{{$order->start_at}}</p>
                      </div>

                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Tanggal Selesai Rental</p>
                        <p class="fw-bold">{{$order->end_at}}</p>
                      </div>

                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Kendaraan</p>
                        <p class="fw-bold">{{$order->mobil->merek}} {{$order->mobil->tipe}} {{$order->mobil->warna}}</p>
                      </div>

                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Tanggal Durasi Rental</p>
                        <p class="fw-bold">{{$order->durasi}} Jam</p>
                      </div>

                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Driver</p>
                        <p class="fw-bold">
                            @if ($order->driver == 0)
                                Tanpa Driver
                            @else
                              {{$order->driverfee->judul}} 
                            @endif
                         
                        </p>
                      </div>

                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Driver Durasi</p>
                        <p class="fw-bold">
                          @if ($order->driver == 0)
                              0
                          @else
                            {{$order->driverfee->durasi}} 
                          @endif
                         
                        </p>
                      </div>
                    </div>

                    <hr class="my-4">
                  

                    <h4 class="mb-3">Data Pemesan</h4>

                    <div class="row">
                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">NIK</p>
                        <p class="fw-bold">{{$order->nik}}</p>
                      </div>

                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Nama</p>
                        <p class="fw-bold">{{$order->nama}}</p>
                      </div>

                      <div class="col-md-6">
                        <p class="m-0 text-muted text-sm">Nomor Whatsapp</p>
                        <p class="fw-bold">{{$order->wa}}</p>
                      </div>

                      <div class="col-md-12">
                        <p class="m-0 text-muted text-sm">Alamat Saat ini</p>
                        <p class="fw-bold">{{$order->alamat}}</p>
                      </div>
                    </div>

                  
                    
          
                   

                   
                    <hr class="my-4">

                    <h4 class="mb-3">Dokumen</h4>

                    <div class="row">
                      <div class="col-md-3">
                        <a href="{{asset('storage/'.$order->file_ktp)}}">
                          <img src="{{asset('storage/'.$order->file_ktp)}}" alt="" class="w-100">
                        </a>
                      </div>
                      <div class="col-md-3">
                        <a href="{{asset('storage/'.$order->file_selfie)}}">
                          <img src="{{asset('storage/'.$order->file_selfie)}}" alt="" class="w-100">
                        </a>
                      </div>
                    </div>
          
                   
                  
                </div>

               
              </div>

             

              
              
        </div>

        @else

        <div class="card-body">
          <div class="py-5 text-center">
              {{-- <img class="d-block mx-auto mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> --}}
              <h2>Pesananmu tidak ditemukan.</h2>
              
            </div>
        </div>


        @endif
    </div>
    <script>

        var durasi = 6;
        var jasaName;
        var biaya = 0;

        $('#jasa').on('change', function () {
            var jasa = this.value;
 

            const myArray = jasa.split(",");

            biaya = parseInt(myArray[1]);
            jasaName = myArray[0];

            // console.log(myArray[0]);
            // console.log("ini"+myArray[1]);

            // ongkir = parseInt(myArray[1]);

            // document.getElementById("ongkir").classList.remove("d-none");

            // document.getElementById("jumlahOngkir").innerHTML = parseInt(myArray[1]).toLocaleString('id-ID');

            // document.getElementById("kurirPaket").innerHTML = getKurir+' '+myArray[0];

            // payment()

            getRincian();
  
        });

        $('#durasi').on('change', function () {
            durasi = this.value;
 

          
            getRincian();
  
        });

        function getRincian(){
            console.log('durasi '+durasi);
            console.log('biaya '+biaya);
            console.log('jasa '+jasaName);

            var tb = biaya*durasi;

            document.getElementById("rincian").classList.remove("d-none");
            document.getElementById("jasaName").innerHTML = jasaName;
            document.getElementById("jasaDurasi").innerHTML = durasi;
            document.getElementById("totalBiaya").innerHTML = tb;
            document.getElementById("totalBayar").innerHTML = tb;
        }

        var clipboard = new ClipboardJS('.btn-copy');

        clipboard.on('success', function(e) {
            console.info('Action:', e.action);
            console.info('Text:', e.text);
            console.info('Trigger:', e.trigger);
            console.info('Trigger:', e.trigger.getAttribute('aria-label'));

            var id =e.trigger.getAttribute('aria-label')

            document.getElementById("icon-"+id).innerHTML = '<i class="bi bi-check h-5"></i>';
            document.getElementById("copy-text-"+id).innerHTML = 'Tercopy';

            setTimeout(() => {
              document.getElementById("icon-"+id).innerHTML = '<i class="bi bi-clipboard-fill text-sm"></i>';
              document.getElementById("copy-text-"+id).innerHTML = 'Copy NoRek';
            }, "3000");

            e.clearSelection();
        });
              
    </script>
@endsection
