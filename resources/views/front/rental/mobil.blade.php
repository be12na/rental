@extends('shared_pages.front2-layout')
@section('content')
    <div class="card">
        {{-- <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div> --}}
        <div class="card-body">
            
            <div class="row">
                <div class="col-md-6">

                    <div id="carouselExampleControls" class="carousel slide h-100 " data-ride="carousel">
                        <div class="carousel-inner h-100">
                          <div class="carousel-item active h-100">
                            <div class=" h-100" style="background-image: url(https://www.wessexfleet.co.uk/assets/images/mercedes-for-company-car.jpg);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                            
                          </div>
                          <div class="carousel-item h-100" >
                            <div class=" h-100" style="background-image: url(https://5.imimg.com/data5/XP/BU/GLADMIN-52001486/tata-tigor-car-500x500.png);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                            
                          </div>

                          <div class="carousel-item h-100" >
                            <div class=" h-100" style="background-image: url(https://www.wessexfleet.co.uk/assets/images/tesla-for-company-car-personal-use.jpg);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                           
                          </div>

                          
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </a>
                      </div>

                    {{-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="https://www.wessexfleet.co.uk/assets/images/mercedes-for-company-car.jpg" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="https://5.imimg.com/data5/XP/BU/GLADMIN-52001486/tata-tigor-car-500x500.png" class="d-block w-100" alt="...">
                          </div>

                          <div class="carousel-item">
                            <img src="https://www.wessexfleet.co.uk/assets/images/tesla-for-company-car-personal-use.jpg" class="d-block w-100" alt="...">
                          </div>
                        </div>
                    </div> --}}

                </div>
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-md-6">
                            <p class="m-0">Small</p>
                            <h3>Toyota Yaris</h3>

                            <div class="row g-3 mt-4">

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Kursi</h6>
                                                <h6 class="font-extrabold mb-0">4</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Transmisi</h6>
                                                <h6 class="font-extrabold mb-0">Manual</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">BBM</h6>
                                                <h6 class="font-extrabold mb-0">Bensin</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Full Tank</h6>
                                                <h6 class="font-extrabold mb-0">25L</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-6">
                           <div class="d-flex justify-content-center align-items-center h-100">
                                <div>
                                    <p class="m-0">Rental mulai</p>
                                    <h3>Rp120.000</h3>

                                    <div class="text-center mt-5">
                                        <a href="{{route('rental.pemesanan')}}" class=" btn btn-primary rounded-pill">Buat Pesanan</a>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>

                </div>
            </div>
             
        </div>

        <div class="card-body">
            
            <div class="row">
                <div class="col-md-6">

                    <div id="carouselExampleControls" class="carousel slide h-100 " data-ride="carousel">
                        <div class="carousel-inner h-100">
                          <div class="carousel-item active h-100">
                            <div class=" h-100" style="background-image: url(https://www.wessexfleet.co.uk/assets/images/mercedes-for-company-car.jpg);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                            
                          </div>
                          <div class="carousel-item h-100" >
                            <div class=" h-100" style="background-image: url(https://5.imimg.com/data5/XP/BU/GLADMIN-52001486/tata-tigor-car-500x500.png);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                            
                          </div>

                          <div class="carousel-item h-100" >
                            <div class=" h-100" style="background-image: url(https://www.wessexfleet.co.uk/assets/images/tesla-for-company-car-personal-use.jpg);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                           
                          </div>

                          
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </a>
                      </div>

                    {{-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="https://www.wessexfleet.co.uk/assets/images/mercedes-for-company-car.jpg" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="https://5.imimg.com/data5/XP/BU/GLADMIN-52001486/tata-tigor-car-500x500.png" class="d-block w-100" alt="...">
                          </div>

                          <div class="carousel-item">
                            <img src="https://www.wessexfleet.co.uk/assets/images/tesla-for-company-car-personal-use.jpg" class="d-block w-100" alt="...">
                          </div>
                        </div>
                    </div> --}}

                </div>
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-md-6">
                            <p class="m-0">Small</p>
                            <h3>Toyota Yaris</h3>

                            <div class="row g-3 mt-4">

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Kursi</h6>
                                                <h6 class="font-extrabold mb-0">4</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Transmisi</h6>
                                                <h6 class="font-extrabold mb-0">Manual</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">BBM</h6>
                                                <h6 class="font-extrabold mb-0">Bensin</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Full Tank</h6>
                                                <h6 class="font-extrabold mb-0">25L</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-6">
                           <div class="d-flex justify-content-center align-items-center h-100">
                                <div>
                                    <p class="m-0">Rental mulai</p>
                                    <h3>Rp120.000</h3>

                                    <div class="text-center mt-5">
                                        <a href="{{route('rental.pemesanan')}}" class=" btn btn-primary rounded-pill">Buat Pesanan</a>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>

                </div>
            </div>
             
        </div>

        <div class="card-body">
            
            <div class="row">
                <div class="col-md-6">

                    <div id="carouselExampleControls" class="carousel slide h-100 " data-ride="carousel">
                        <div class="carousel-inner h-100">
                          <div class="carousel-item active h-100">
                            <div class=" h-100" style="background-image: url(https://www.wessexfleet.co.uk/assets/images/mercedes-for-company-car.jpg);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                            
                          </div>
                          <div class="carousel-item h-100" >
                            <div class=" h-100" style="background-image: url(https://5.imimg.com/data5/XP/BU/GLADMIN-52001486/tata-tigor-car-500x500.png);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                            
                          </div>

                          <div class="carousel-item h-100" >
                            <div class=" h-100" style="background-image: url(https://www.wessexfleet.co.uk/assets/images/tesla-for-company-car-personal-use.jpg);background-size: cover;background-position: center;background-repeat: no-repeat;">
                                
                            </div>
                           
                          </div>

                          
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </a>
                      </div>

                    {{-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="https://www.wessexfleet.co.uk/assets/images/mercedes-for-company-car.jpg" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="https://5.imimg.com/data5/XP/BU/GLADMIN-52001486/tata-tigor-car-500x500.png" class="d-block w-100" alt="...">
                          </div>

                          <div class="carousel-item">
                            <img src="https://www.wessexfleet.co.uk/assets/images/tesla-for-company-car-personal-use.jpg" class="d-block w-100" alt="...">
                          </div>
                        </div>
                    </div> --}}

                </div>
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-md-6">
                            <p class="m-0">Small</p>
                            <h3>Toyota Yaris</h3>

                            <div class="row g-3 mt-4">

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Kursi</h6>
                                                <h6 class="font-extrabold mb-0">4</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Transmisi</h6>
                                                <h6 class="font-extrabold mb-0">Manual</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">BBM</h6>
                                                <h6 class="font-extrabold mb-0">Bensin</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card bg-light m-0  border"  >
                                        
                                        <div class="card-body">
                                            <div class="">
                                                <h6 class="text-muted font-semibold">Full Tank</h6>
                                                <h6 class="font-extrabold mb-0">25L</h6>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-6">
                           <div class="d-flex justify-content-center align-items-center h-100">
                                <div>
                                    <p class="m-0">Rental mulai</p>
                                    <h3>Rp120.000</h3>

                                    <div class="text-center mt-5">
                                        <a href="{{route('rental.pemesanan')}}" class=" btn btn-primary rounded-pill">Buat Pesanan</a>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>

                </div>
            </div>
             
        </div>
    </div>
    <script>

        var durasi = 6;
        var jasaName;
        var biaya = 0;

        $('#jasa').on('change', function () {
            var jasa = this.value;
 

            const myArray = jasa.split(",");

            biaya = parseInt(myArray[1]);
            jasaName = myArray[0];

            // console.log(myArray[0]);
            // console.log("ini"+myArray[1]);

            // ongkir = parseInt(myArray[1]);

            // document.getElementById("ongkir").classList.remove("d-none");

            // document.getElementById("jumlahOngkir").innerHTML = parseInt(myArray[1]).toLocaleString('id-ID');

            // document.getElementById("kurirPaket").innerHTML = getKurir+' '+myArray[0];

            // payment()

            getRincian();
  
        });

        $('#durasi').on('change', function () {
            durasi = this.value;
 

          
            getRincian();
  
        });

        function getRincian(){
            console.log('durasi '+durasi);
            console.log('biaya '+biaya);
            console.log('jasa '+jasaName);

            var tb = biaya*durasi;

            document.getElementById("rincian").classList.remove("d-none");
            document.getElementById("jasaName").innerHTML = jasaName;
            document.getElementById("jasaDurasi").innerHTML = durasi;
            document.getElementById("totalBiaya").innerHTML = tb;
            document.getElementById("totalBayar").innerHTML = tb;
        }
       
    </script>
@endsection
