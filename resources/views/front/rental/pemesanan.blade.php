@extends('shared_pages.front2-layout')
@section('content')
    <div class="card">
        {{-- <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div> --}}
        <div class="card-body">
            <div class="py-5 text-center">
                {{-- <img class="d-block mx-auto mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> --}}
                <h2>Form Pemesanan</h2>
                <p class="lead">Silahkan Isi Form Pemesanan dibawah ini dengan Lengkap dan Benar.</p>
              </div>

              <form  action="{{route('pemesanan.store')}}" method="post" enctype="multipart/form-data">
                @csrf
          
              <div class="row g-5">
               
                <div class="col-md-7 col-lg-8">
                  

                    <h4 class="mb-3">Pilih Durasi</h4>

                    
                    <div class="row g-3">
                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="durasi" class="btn-check" id="btn-check" autocomplete="off" value="6">
                          <label class="btn btn-outline-primary" for="btn-check">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">6 Jam</p>
                              <p class="m-0 fw-bold">Rp150.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="durasi" class="btn-check" id="btn-check-2" autocomplete="off" value="12">
                          <label class="btn btn-outline-primary" for="btn-check-2">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">12 Jam</p>
                              <p class="m-0 fw-bold">Rp250.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="durasi" class="btn-check" id="btn-check-3" autocomplete="off" value="24">
                          <label class="btn btn-outline-primary" for="btn-check-3">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">24 Jam</p>
                              <p class="m-0 fw-bold">Rp350.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="durasi" class="btn-check" id="btn-check-4" autocomplete="off" value="day">
                          <label class="btn btn-outline-primary" for="btn-check-4">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">Perday</p>
                              <p class="m-0 fw-bold">Rp325.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>
                      

                     
                    </div>

                    <hr class="my-4">

                    <h4 class="mb-3">Pilih Driver</h4>

                    <div class="row g-3">
                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="driver" class="btn-check" id="btn-check-driver" autocomplete="off" value="6">
                          <label class="btn btn-outline-primary" for="btn-check-driver">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">Dalam kota 12 Jam</p>
                              <p class="m-0 fw-bold">Rp125.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="driver" class="btn-check" id="btn-check-driver-2" autocomplete="off" value="12">
                          <label class="btn btn-outline-primary" for="btn-check-driver-2">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">Dalam kota 24 Jam</p>
                              <p class="m-0 fw-bold">Rp175.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="driver" class="btn-check" id="btn-check-driver-3" autocomplete="off" value="24">
                          <label class="btn btn-outline-primary" for="btn-check-driver-3">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">Luar kota Jateng 12 Jam</p>
                              <p class="m-0 fw-bold">Rp150.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="driver" class="btn-check" id="btn-check-driver-4" autocomplete="off" value="day">
                          <label class="btn btn-outline-primary" for="btn-check-driver-4">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">Luar kota Jateng 24 Jam</p>
                              <p class="m-0 fw-bold">Rp250.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="driver" class="btn-check" id="btn-check-driver-5" autocomplete="off" value="day">
                          <label class="btn btn-outline-primary" for="btn-check-driver-5">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">Luar Provinsi 24 Jam</p>
                              <p class="m-0 fw-bold">Rp300.000</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>

                      <div class="col-md-6 ">
                        <div class="d-grid">
                          <input type="radio" name="driver" class="btn-check" id="btn-check-driver-6" autocomplete="off" value="day">
                          <label class="btn btn-outline-primary" for="btn-check-driver-6">
                            <div class="d-flex justify-content-between p-2">
                              <p class="m-0">Tanpa Driver</p>
                              <p class="m-0 fw-bold">Rp0</p>
                            </div>
                          </label>
                        </div>
                        

                      </div>
                      

                     
                    </div>

                    <hr class="my-4">

                    <h4 class="mb-3">Data Pemesan</h4>

                    <div class="my-3">
                     
                      <div class="row g-3 mb-3">
                        <div class="col-md-12">
                          <label for="nik" class="form-label">NIK KTP</label>
                          <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" placeholder="" value="{{old('nik')}}">
                          @error('nik')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                          @enderror
                        </div>

                        <div class="col-md-6">
                          <label for="nama" class="form-label">Nama (Sesuai Identitas)</label>
                          <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="" value="{{old('nama')}}"  >
                        
                          @error('nama')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                          @enderror
                          
                        </div>

                        <div class="col-md-6">
                          <label for="wa" class="form-label">Nomor Whatsapp</label>
                          <input type="text" class="form-control @error('wa') is-invalid @enderror" id="wa" name="wa"  placeholder="" value="{{old('wa')}}" >
                          @error('wa')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                          @enderror
                        </div>

                        
                        <div class="col-md-12">
                          <label for="alamat" class="form-label">Alamat saat ini</label>
                          <textarea class="form-control @error('alamat') is-invalid @enderror" id="alamat" rows="3" name="alamat">{{old('alamat')}}</textarea>
                          @error('alamat')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                          @enderror
                        </div>
                      </div>
                     
                    </div>

                    <hr class="my-4">
                  
                    <h4 class="mb-3">Detail Pemesanan</h4>
                    
                    <div class="row g-3">

                        <div class="col-md-6">
                            <label for="mulai" class="form-label">Tanggal Mulai Rental</label>
                            <input type="date" class="form-control @error('mulai') is-invalid @enderror" id="mulai" name="mulai" min="{{date("Y-m-d")}}" placeholder="" value="{{old('mulai')}}" >
                            @error('mulai')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label for="selesai" class="form-label">Tanggal Selesai Rental</label>
                            <input type="date" class="form-control @error('selesai') is-invalid @enderror" id="selesai" name="selesai" min="{{date("Y-m-d")}}" placeholder="" value="{{old('selesai')}}" >
                            @error('selesai')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                    </div>
          

       

                    <hr class="my-4">
          
                    <h4 class="mb-3">Upload Dokumen</h4>
          
                    <div class="mb-3">
                      <label for="formFile" class="form-label">Identitas KTP/SIM</label>
                      <input class="form-control @error('file_ktp') is-invalid @enderror" type="file" name="file_ktp" id="formFile">
                      @error('file_ktp')
                        <div class="text-danger mt-1" style="font-size: .875em;">
                            {{ $message }}
                        </div>
                      @enderror
                    </div>
                    
                    <div class="mb-3">
                      <label for="formFile" class="form-label">Identitas + Selfie</label>
                      <input class="form-control @error('file_selfie') is-invalid @enderror" type="file" name="file_selfie" id="formFile">
                      @error('file_selfie')
                        <div class="text-danger mt-1" style="font-size: .875em;">
                            {{ $message }}
                        </div>
                      @enderror
                    </div>

                    <div class="mt-3">
                      <div class="form-check">
                        <input class="form-check-input @error('agree')  border border-danger border-2 @enderror" type="checkbox" name="agree" value="1" id="flexCheckChecked" >
                        <label class="form-check-label @error('agree') text-danger @enderror" for="flexCheckChecked">
                          Dengan ini saya Menyetujui Syarat dan Ketentuan Sistem Rental
                        </label>
                      </div>
                      @error('agree')
                      <div class="text-danger mt-1" style="font-size: .875em;">
                          Kamu harus setujui ketentuan kami
                      </div>
                      @enderror
                    </div>
                     
                  
          
                   
          
                   
                    <hr class="my-4">
          
                    <button class="w-100 btn btn-primary btn-lg d-md-block d-none" type="submit">Buat Pemesanan</button>
                  
                </div>

                <div class="col-md-5 col-lg-4 order-md-last">
                  <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-primary">Rincian Pemesanan</span>
                    {{-- <span class="badge bg-primary rounded-pill">3</span> --}}
                  </h4>

                  <div class="my-3">

                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Promo code">
                      <button type="submit" class="btn btn-secondary">Redeem</button>
                    </div>
                  
                  </div>

                  <div class="mb-3">
                    <label for="kode" class="form-label">Kode Marketing (Kosongkan Jika Tidak Ada)</label>
                    <input type="text" class="form-control" id="kode" name="kode" placeholder="" value="{{old('kode')}}" >
                  
                      
                   
                  </div>

                  <hr class="my-4">

                  <div class="row g-1 align-items-center mb-3">

                    <div class="col-md-2 ">
                      <div class="w-100 rounded" style="background-image: url(https://www.wessexfleet.co.uk/assets/images/mercedes-for-company-car.jpg);background-size: cover;background-position: center;background-repeat: no-repeat; height: 40px;">

                      </div>
                      
                    </div>
                    <div class="col-md-10">
                      <div class="d-flex justify-content-between ms-2">

                        <span>Toyota Yaris</span>
                        <strong>Rp124.000</strong>
    
                      </div>
                    </div>

                  </div>

                  <div class="d-flex justify-content-between mb-3 d-none" id="rincian">

                    <div>
                      <h6 class="my-0" id="jasaName"></h6>
                      <small class="text-body-secondary"><span id="jasaDurasi"></span> jam</small>
                    </div>
                    <span class="text-body-secondary">Rp<span id="totalBiaya"></span></span>

                  </div>

                  {{-- <div class="d-flex justify-content-between">

                    <div class="text-success">
                      <h6 class="my-0">Promo code</h6>
                      <small>EXAMPLECODE</small>
                    </div>
                    <span class="text-success">−$5</span>

                  </div> --}}

                  <div class="d-flex justify-content-between mb-3">

                    <span>Total</span>
                    <strong>Rp<span id="totalBayar">0</span></strong>

                  </div>


                  {{-- <ul class="list-group mb-3">
                    <li id="rincian" class="list-group-item d-flex justify-content-between lh-sm d-none">
                      <div>
                        <h6 class="my-0" id="jasaName"></h6>
                        <small class="text-body-secondary"><span id="jasaDurasi"></span> jam</small>
                      </div>
                      <span class="text-body-secondary">Rp<span id="totalBiaya"></span></span>
                    </li>
                   
                    <li class="list-group-item d-flex justify-content-between lh-sm">
                      <div class="text-success">
                        <h6 class="my-0">Promo code</h6>
                        <small>EXAMPLECODE</small>
                      </div>
                      <span class="text-success">−$5</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between">
                      <span>Total</span>
                      <strong>Rp<span id="totalBayar">0</span></strong>
                    </li>
                  </ul> --}}

                 
          
                  
                </div>
              </div>

              <div class="d-block d-md-none">

                <hr class="my-4 ">

                <button class="w-100 btn btn-primary btn-lg mt-3" type="submit">Buat Pemesanan</button>


              </div>

              
              </form> 
        </div>
    </div>
    <script>

        var durasi = 6;
        var jasaName;
        var biaya = 0;

        $('#jasa').on('change', function () {
            var jasa = this.value;
 

            const myArray = jasa.split(",");

            biaya = parseInt(myArray[1]);
            jasaName = myArray[0];

            // console.log(myArray[0]);
            // console.log("ini"+myArray[1]);

            // ongkir = parseInt(myArray[1]);

            // document.getElementById("ongkir").classList.remove("d-none");

            // document.getElementById("jumlahOngkir").innerHTML = parseInt(myArray[1]).toLocaleString('id-ID');

            // document.getElementById("kurirPaket").innerHTML = getKurir+' '+myArray[0];

            // payment()

            getRincian();
  
        });

        $('#durasi').on('change', function () {
            durasi = this.value;
 

          
            getRincian();
  
        });

        function getRincian(){
            console.log('durasi '+durasi);
            console.log('biaya '+biaya);
            console.log('jasa '+jasaName);

            var tb = biaya*durasi;

            document.getElementById("rincian").classList.remove("d-none");
            document.getElementById("jasaName").innerHTML = jasaName;
            document.getElementById("jasaDurasi").innerHTML = durasi;
            document.getElementById("totalBiaya").innerHTML = tb;
            document.getElementById("totalBayar").innerHTML = tb;
        }
       
    </script>
@endsection
