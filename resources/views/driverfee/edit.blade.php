@extends('shared_pages.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('driverfee.update',$data->id) }}" name="myForm" method="POST" onsubmit="return validateForm()">
                @csrf
                @method('PATCH')
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="d-sm-flex">
                        <a href="{{ route('driverfee.index') }}" class="btn btn-md btn-circle btn-outline-danger">
                            <i class="bi bi-skip-backward-fill"></i> Back
                        </a>
                        &nbsp;
                    </div>
                    {{-- <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                        Submit Order <i class="bi bi-sd-card-fill"></i>
                    </button> --}}
                </div>

                <!-- content -->
                <div class="row">
             
                    <div class="col-lg-12">
                        <div class="card border-bottom-primary shadow mb-4">
                            <div class="card-header py-3 bg-orange">
                                <h6 class="m-0 font-weight-bold text-white">Data harga Driver </h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                  
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Judul</label>
                                            <input type="text" class=" form-control @error('judul') is-invalid @enderror" name="judul" value="{{ old('judul') ? old('judul') : $data->judul }}">
                                            @error('judul')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Durasi</label>
                                            <input type="text" class=" form-control @error('durasi') is-invalid @enderror" name="durasi" value="{{ old('durasi') ? old('durasi') : $data->durasi }}">
                                            @error('durasi')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Harga</label>
                                            <input type="text" class=" form-control @error('harga') is-invalid @enderror" name="harga" value="{{ old('harga') ? old('harga') : $data->harga }}">
                                            @error('harga')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Urutan</label>
                                            <input type="text" class=" form-control @error('order') is-invalid @enderror" name="order" value="{{ old('order') ? old('order') : $data->order }}">
                                            @error('order')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                  
                                </div>

                                <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                                    Submit Jasa <i class="bi bi-sd-card-fill"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </form>
        </div>
    </div>
    <script>
       
    </script>
@endsection
