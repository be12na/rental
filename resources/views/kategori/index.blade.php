@extends('shared_pages.layout')
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header text-end">
                <h4>{{ $dt['title'] }}</h4>
            </div>
            <div class="card-body">
                @if ($message = Session::get('success'))
                    <div class="mb-3 pl-3 pr-3">
                        <div class="alert alert-success alert-dismissible show fade">
                            <i class="far fa-fw fa-bell"></i> {{ $message }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                @endif
                @can('peminjaman-create')
                    <a href="{{ route('kategori.'.$tipeKat.'.create') }}" class="btn btn-outline-success mb-5">
                        <i class="bi bi-plus-circle"></i> Buat Kategori Baru
                    </a>
                @endcan
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <tr class="bg-orange text-light">
                                <th>No</th>
                                <th>Nama</th>
                             
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $k = 0; ?>
                            @forelse ($dataList as $p)
                                <tr>
                                    <td>{{ $p->order }}</td>
                                   
                                   
                                    <td>{{ $p->nama }}</td>
                                   
                                   
                                   
                                    
                                    <td>

                                        <div class="d-flex">

                                            <a class="btn btn-sm btn-primary me-1" href="{{ route('kategori.'.$tipeKat.'.edit', $p->id) }}"
                                                title="Edit Customer"><i class="bi bi-pencil"></i></a>
                                           
                                            <form onsubmit="return confirm('Benar akan meghapus data ini ?')"
                                                action="{{ route('kategori.'.$tipeKat.'.destroy', $p->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                
                                                    <button type="submit" class="btn btn-sm btn-danger" title="Delete"><i
                                                            class="bi bi-x"></i></button>
                                                
                                            </form>

                                        </div>
                                        
                                    </td>
                                </tr>
                                <?php $k++; ?>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-danger text-center">Data order rental belum ada</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script>
       
    </script>
@endSection
