@extends('shared_pages.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{ $dt['title'] }}</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('transaksi.'.$tipeKat.'.store') }}" name="myForm" method="POST" onsubmit="return validateForm()">
                @csrf
                @method('POST')
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="d-sm-flex">
                        <a href="{{ route('transaksi.'.$tipeKat.'.index') }}" class="btn btn-md btn-circle btn-outline-danger">
                            <i class="bi bi-skip-backward-fill"></i> Back
                        </a>
                        &nbsp;
                    </div>
                    {{-- <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                        Submit Order <i class="bi bi-sd-card-fill"></i>
                    </button> --}}
                </div>

                {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

                <!-- content -->
                <div class="row">
             
                    <div class="col-lg-12">
                        <div class="card border-bottom-primary shadow mb-4">
                            <div class="card-header py-3 bg-orange">
                                <h6 class="m-0 font-weight-bold text-white">Data Transaksi Pemasukan</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                   
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Tanggal</label>
                                            <input type="date" class=" form-control @error('tanggal') is-invalid @enderror" name="tanggal" value="{{ old('tanggal') }}">
                                            @error('tanggal')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="mt-2">Kategori</label>
                                            <select class="form-select @error('kategori') is-invalid @enderror" id="kategori" name="kategori" >
                                                <option value="">Choose...</option>
                                                @foreach ($kategoriList as $item)
                                                    <option {{old('kategori') == $item->id ? 'selected' : ''}} value="{{$item->id}}">{{$item->nama}}</option>
                                                @endforeach
                                            
                                            </select>
                                            @error('order')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="mt-2">Total Transaksi</label>
                                            <input type="number" class=" form-control @error('total') is-invalid @enderror" name="total" value="{{ old('total') }}">
                                            @error('total')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12">

                                        <div class="row align-items-end">

                                            <div class="col-lg-6" id="cusSelectForm">

                                                <div class="form-group">
                                                    <label class="mt-2">Customer</label>
                                                    <select class="form-select @error('customer') is-invalid @enderror" id="customer" name="customer" >
                                                        <option value="">Choose...</option>
                                                        @foreach ($customerList as $item)
                                                            <option {{old('customer') == $item->id ? 'selected' : ''}} value="{{$item->id}}">{{$item->nama}}</option>
                                                        @endforeach
                                                    
                                                    </select>
                                                    @error('order')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>

                                            </div>
                                            <div class="col-lg-6">

                                                <div class="form-group" id="customerButtonPilih">
                                                    <input  name="customerForm" id="customerForm" value="1" type="radio" class="btn-check"  autocomplete="off" {{old('customerForm') == 1 ? 'checked' : ""}} >
                                                    <label class="btn btn-primary" for="customerForm">Buat Customer</label>
                                                </div>

                                                <div class="form-group d-none" id="customerButtonInput">
                                                    <input  name="customerForm" id="customerFormInput" value="2" type="radio" class="btn-check"  autocomplete="off" {{old('customerForm') == 2 ? 'checked' : (!old('customerForm') ? 'checked' :'' )}} >
                                                    <label class="btn btn-primary" for="customerFormInput">Pilih Customer</label>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="d-none" id="cusInputForm">
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label>NIK</label>
                                                    <div class="input-group mb-3">
                                                        
                                                        <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik"
                                                            value="{{ old('nik') }}" placeholder="NIK customer" autofocus>
                                                        @error('nik')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="row form-group ">
                                                <div class="col-sm-12">
                                                    <label>Nama Customer</label>
                                                    <div class="input-group mb-3">
                                                        
                                                        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                                            value="{{ old('nama') }}" placeholder="Nama customer" autofocus>
                                                        @error('nama')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label>Telp</label>
                                                    <div class="input-group mb-3">
                                                        
                                                        <input type="text" class="form-control @error('telp') is-invalid @enderror" name="telp"
                                                            value="{{ old('telp') }}">
                                                        @error('telp')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label>Kota</label>
                                                    <div class="input-group mb-3">
                                                        
                                                        <select type="text" class="form-control @error('kota') is-invalid @enderror"
                                                            name="kota" value="{{ old('kota') }}">
                                                            <option value="">-- Pilih Kota --</option>
                                                            @foreach ($kotaList as $p)
                                                                <option value="{{ $p->id }}">{{ $p->kota }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('kota')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                            
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label>Alamat Customer</label>
                                                    <div class="input-group mb-3">
                                                        
                                                        <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                                                            value="{{ old('alamat') }}" id="formFile">
                                                        @error('alamat')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="mt-2">Keterangan</label>
                                            <textarea class="form-control" id="keterangan" rows="3" name="keterangan">{{old('keterangan')}}</textarea>
                                        </div>
                                    </div>
                                    
                                  
                                </div>

                                <button type="submit" class="btn btn-outline-success btn-md btn-icon-split">
                                    Submit Transaksi <i class="bi bi-sd-card-fill"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </form>
        </div>
    </div>

    @php
        if(old('customerForm') == null){
            $cf = 0;
        }else{
            $cf = old('customerForm');
        }
    @endphp

    {{-- {{$cf}} --}}

    <script>

       

        $(document).ready(function() {
            

           

                if({{$cf}} == 1){

                document.getElementById("customerButtonPilih").classList.add("d-none");
                document.getElementById("customerButtonInput").classList.remove("d-none");
                document.getElementById("cusInputForm").classList.remove("d-none");
                document.getElementById("cusSelectForm").classList.add("d-none");

                }

                if({{$cf}} == 2){

               
                    document.getElementById("customerButtonPilih").classList.remove("d-none");
                    document.getElementById("customerButtonInput").classList.add("d-none");
                    document.getElementById("cusInputForm").classList.add("d-none");
                    document.getElementById("cusSelectForm").classList.remove("d-none");

                }

            
            

           

        });

        $('#customerForm').on('change', function () {

            var valueCus = this.value;

            console.log(valueCus);
            
        

                document.getElementById("customerButtonPilih").classList.add("d-none");
                document.getElementById("customerButtonInput").classList.remove("d-none");
                document.getElementById("cusInputForm").classList.remove("d-none");
                document.getElementById("cusSelectForm").classList.add("d-none");
                
           

            
            
        })

        $('#customerFormInput').on('change', function () {

            var valueCus = this.value;

            console.log(valueCus);

           
                
            

                document.getElementById("customerButtonPilih").classList.remove("d-none");
                document.getElementById("customerButtonInput").classList.add("d-none");
                document.getElementById("cusInputForm").classList.add("d-none");
                document.getElementById("cusSelectForm").classList.remove("d-none");

         



        })
       
    </script>
@endsection
