<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;


class Pemesanan extends Model
{
    use HasFactory;

    public function jasa(): HasOne
    {
        return $this->hasOne(Jasa::class, 'id', 'jasa_id');
    }

    public function mobil(): HasOne
    {
        return $this->hasOne(Mobil::class, 'id', 'kendaraan_id');
    }

    public function driverfee(): HasOne
    {
        return $this->hasOne(Driverfee::class, 'id', 'driver');
    }
}
