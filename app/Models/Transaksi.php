<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;


class Transaksi extends Model
{
    use HasFactory;

    protected $dates = [
        'tanggal',
       
    ];

    public function kategori(): HasOne
    {
        return $this->hasOne(Kategoritransaksi::class, 'id', 'kategori_id');
    }

    public function customer(): HasOne
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}
