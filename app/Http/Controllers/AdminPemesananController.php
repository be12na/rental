<?php

namespace App\Http\Controllers;

use App\Models\Pemesanan;
use Illuminate\Http\Request;

class AdminPemesananController extends Controller
{
    public function index()
    {
        $dataList = Pemesanan::orderBy('start_at','desc')->get();
      
        $dt = [
            'menu' => 'Data Pemesanan',
            'title' => 'List Pemesanan',
            
        ];
       
        return view('pemesanan.index', compact('dt', 'dataList'));
    }

    public function detail($id)
    {
        $order = Pemesanan::with('driverfee','mobil')->where('ordernumber',$id)->first();
      
        $dt = [
            'menu' => 'Data Pemesanan',
            'title' => 'Detail Pemesanan',
            
        ];
       
        return view('pemesanan.detail', compact('dt','order'));
    }

    
}
