<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Driverfee;
use App\Models\Jasa;
use App\Models\Menu;
use App\Models\Mobil;
use App\Models\Pemesanan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FrontController extends Controller
{
   
    public function pemesanan()
    {
        
        $dt = [
            'menu' => 'Pemesanan',
            'submenu' => '',
            'title' => 'Pemesanan'
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];


        $mobilList = Mobil::all();
        $driverList = Driverfee::orderBy('order','asc')->get();
        

        return view('front.pemesanan', compact('dt', 'menu','mobilList','driverList'));
    }

    public function storePemesanan(Request $request)
    {
      

        $rules = [

            'nik'      => 'required|numeric|digits:16',
            'nama'      => 'required',
            'wa'      => 'required',
            'alamat'      => 'required',
            
            'mulai'     => 'required',
            'selesai'     => 'required',
            'durasi'     => 'required',
           
            'kendaraan'     => 'required',
            'driver'     => 'required',
            'agree' => 'required',

            'file_ktp'     => 'required|image|file|max:2048',
            'file_selfie'     => 'required|image|file|max:2048',
            
            
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'numeric' => 'Nomor NIK harus diisi dengan angka',
            'digits' => 'Nomor NIK harus  berjumlah 16 angka',
            'image' => 'Dokumen harus file gambar',
            'max' => 'Dokumen setidaknya maksimal 2MB'
        ];

        $this->validate($request, $rules, $errorMessages);

        $telp = $request->wa;

        $nta = Str::is('0*', $telp);
        $ntb = Str::is('62*', $telp);

        if($nta){
            $slice = Str::after($telp,'0');
            $wa = '62'.$slice;
        }

        if($ntb){
            $wa = $telp;
        }

     

        $durasiArray = explode(',',$request->durasi);
        $kendaraanArray = explode(',',$request->kendaraan);
        $driverArray = explode(',',$request->driver);

        // $driverTitle = $driverArray[1].' '.$driverArray[2];
        // $driverHarga = $driverArray[3];


        $date = Carbon::now()->toDateString();
        $dateFormat = Carbon::now()->isoFormat('YYYYMMDD');

        $cekOrder = Pemesanan::whereDate('created_at',$date)->count(); 

        $nextNumber = $cekOrder+1;
        $padded = Str::padLeft($nextNumber, 6, '0');

        $orderNumber = $dateFormat.$padded;

        $dataMobil = Mobil::find($kendaraanArray[0]);

        

        $potongan=0;

        if($durasiArray[0] == "6"){
            $hargaRental = $dataMobil->harga_6;
        }elseif($durasiArray[0] == "12"){
            $hargaRental = $dataMobil->harga_12;
        }elseif($durasiArray[0] == "24"){
            $hargaRental = $dataMobil->harga_24;
        }else{
            $hargaRental = $dataMobil->harga_day;
        }

        if($driverArray[0] == 0){
            $driverFee = 0;
        }else{
            $driverFee = $driverArray[3];
        }

       

        $total = $hargaRental+$driverArray[3]-$potongan;

        $data = new Pemesanan();

        $data->ordernumber = $orderNumber;
        $data->nik = $request->nik;
        $data->nama = $request->nama;
        $data->alamat = $request->alamat;
        $data->wa = $wa;
        $data->jasa_id = 0;
        $data->kode_marketing = $request->kode;
        $data->kendaraan_id = $kendaraanArray[0];
        $data->durasi = $durasiArray[0];
        $data->start_at = $request->mulai;
        $data->end_at = $request->selesai;
        $data->driver = $driverArray[0];
        $data->driver_fee = $driverFee;
       
        $data->biaya =  $hargaRental;
        $data->total_bayar =  $total;

        $fileKtp = $request->file('file_ktp')->store('order/'.$orderNumber.'','public');

        $fileSelfie = $request->file('file_selfie')->store('order/'.$orderNumber.'','public');

        $data->file_ktp = $fileKtp;
        $data->file_selfie = $fileSelfie;

        $data->save();



        // return redirect('https://api.whatsapp.com/send?phone='.$data->wa.'&text=Halo%20Admin%20...%20Saya%20udah%20isi%20data%20pesanan%20di%20web.%20Bisa%20tolong%20di%20proses.%20Terima%20kasih');

        return redirect()->route('rental.pemesanan.detail',['order'=>$data->ordernumber]);
    }

    public function detailPemesanan(Request $request)
    {
        $bankList = Bank::all();

        $order = Pemesanan::with('driverfee','mobil')->where('ordernumber',$request->order)->first();

        // dd($order);

        return view('front.detailpemesanan',compact('order','bankList'));
    }
}
