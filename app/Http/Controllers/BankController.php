<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $dataList = Bank::all();
      
        $dt = [
            'menu' => 'Data bank',
            'title' => 'List bank',
            
        ];
       
        return view('bank.index', compact('dt', 'dataList'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $dt = [
            'menu' => 'Kelola Bank',
            'title' => 'Tambah Bank Baru',
            
            
        ];
       
        return view('bank.create', compact('dt'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rules = [
            
            'nama_bank'     => 'required',
            'akun'      => 'required',
            'norek'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $data = new Bank();

        $data->nama_bank = $request->nama_bank;
        $data->akun_bank = $request->akun;
        $data->norek_bank = $request->norek;
      

        $data->save();

        return redirect()->route('bank.index')->with('success', 'Data Bank berhasil disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Bank::find($id);

        $dt = [
            'menu' => 'Kelola bank',
            'title' => 'Edit bank ',
            
        ];
       

        return view('bank.edit', compact('dt','data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Bank::find($id);

        $rules = [
            
            'nama_bank'     => 'required',
            'akun'      => 'required',
            'norek'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

       
        $data->nama_bank = $request->nama_bank;
        $data->akun_bank = $request->akun;
        $data->norek_bank = $request->norek;
      

        $data->save();

        return redirect()->route('bank.index')->with('success', 'Data Bank berhasil disimpan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Bank::find($id);
        $data->delete();

        return redirect()->route('bank.index')->with('success', 'Data bank berhasil dihapus');
    }
}
