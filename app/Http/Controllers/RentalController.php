<?php

namespace App\Http\Controllers;

use App\Models\Jasa;
use App\Models\Mobil;
use Illuminate\Http\Request;

class RentalController extends Controller
{
    public function durasiFetch(Request $request)
    {
        $data = Mobil::find($request->kendaraan_id);

        $arrayDurasi = [
            [
                'waktu' => '6',
                'biaya' => $data->harga_6

            ],
            [
                'waktu' => '12',
                'biaya' => $data->harga_12

            ],
            [
                'waktu' => '24',
                'biaya' => $data->harga_24

            ],
            [
                'waktu' => 'day',
                'biaya' => $data->harga_day

            ]

        ];

        return response()->json($arrayDurasi);

    }

    public function mobil()
    {
        $dt = [
            'menu' => 'Pemesanan',
            'submenu' => '',
            'title' => 'Pemesanan'
        ];

        return view('front.rental.mobil', compact('dt'));
    }

    public function pemesanan()
    {
        $dt = [
            'menu' => 'Pemesanan',
            'submenu' => '',
            'title' => 'Pemesanan'
        ];
       


        $mobilList = Mobil::all();
        $jasaList = Jasa::all();
        

        return view('front.rental.pemesanan', compact('dt','mobilList','jasaList'));
    }


}
