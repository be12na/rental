<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\User;
use App\Models\Customer;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kota;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:customer-list|customer-create|customer-edit|customer-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:customer-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:customer-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:customer-delete', ['only' => ['destroy']]);
    }
    public function index(): View
    {
        // cek role user yg login
        $customer = [];
        $role = Auth::user()->roles->pluck('name')[0];
        if ($role == 'Admin') {
            $customer = Customer::with('kotaCustomer')->get();
        } else {
            $customer = Customer::with('kotaCustomer')->where('user_id', Auth::user()->id)->get();
        }
        $dt = [
            'menu' => 'Kelola Customer',
            'title' => 'List customer',
            'customer' => $customer,
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];
        return view('customer.index', compact('dt', 'menu'));
    }
    public function create(): View
    {
        // cek role user yg login
       
        $dt = [
            'menu' => 'Kelola Customer',
            'title' => 'Tambah Customer Baru',
           
        ];

        $kotaList = Kota::all();
        
        return view('customer.create', compact('dt', 'kotaList'));
    }
    public function store(Request $request): RedirectResponse
    {
        $rules = [
            'nik'      => 'required|numeric|digits:16',
           
            'nama' => 'required',
            'alamat' => 'required',
            'telp' => 'required',
            'kota' => 'required',
        ];
        
        $errorMessages = [
            'numeric' => 'Nomor NIK harus diisi dengan angka',
            'digits' => 'Nomor NIK harus  berjumlah 16 angka',
            'nik.required' => 'belum diisi',
            'kota.required' => 'belum diisi',
            'nama.required' => 'belum diisi',
            'alamat.required' => 'belum diisi',
            'telp.required' => 'belum diisi',
            'pic.required' => 'belum diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $telp = $request->telp;

        $nta = Str::is('0*', $telp);
        $ntb = Str::is('62*', $telp);

        if($nta){
            $slice = Str::after($telp,'0');
            $wa = '62'.$slice;
        }

        if($ntb){
            $wa = $telp;
        }

        $data = new Customer();

        $data->nik = $request->nik;
        $data->nama = $request->nama;
        $data->alamat = $request->alamat;
        $data->telp = $wa;
        $data->kota_id = $request->kota;
        $data->user_id = 0;

        $data->save();
        
        
        return redirect()->route('customer.index')->with('success', 'Data customer berhasil disimpan');
        
    }
    public function edit(customer $customer): View
    {
        // cek role user yg login
       
        $dt = [
            'menu' => 'Kelola Customer',
            'title' => 'Edit Data customer',
            'customer' => $customer,
            
        ];
        $kotaList = Kota::all();
        return view('customer.edit', compact('dt', 'kotaList'));
    }
    public function update(Request $request, $id): RedirectResponse
        { 
        
            $data = Customer::find($id);
        
            $rules = [
                'nik'      => 'required|numeric|digits:16',
            
                'nama' => 'required',
                'alamat' => 'required',
                'telp' => 'required',
                'kota' => 'required',
            ];
            
            $errorMessages = [
                'numeric' => 'Nomor NIK harus diisi dengan angka',
                'digits' => 'Nomor NIK harus  berjumlah 16 angka',
                'nik.required' => 'belum diisi',
                'kota.required' => 'belum diisi',
                'nama.required' => 'belum diisi',
                'alamat.required' => 'belum diisi',
                'telp.required' => 'belum diisi',
                'pic.required' => 'belum diisi',
            ];

            $this->validate($request, $rules, $errorMessages);

            $telp = $request->telp;

            $nta = Str::is('0*', $telp);
            $ntb = Str::is('62*', $telp);

            if($nta){
                $slice = Str::after($telp,'0');
                $wa = '62'.$slice;
            }

            if($ntb){
                $wa = $telp;
            }

            

            $data->nik = $request->nik;
            $data->nama = $request->nama;
            $data->alamat = $request->alamat;
            $data->telp = $wa;
            $data->kota_id = $request->kota;
            $data->user_id = 0;

            $data->save();
            
            
            return redirect()->route('customer.index')->with('success', 'Data customer berhasil disimpan');

        }
    public function destroy(Customer $customer): RedirectResponse
    {
        $customer->delete();
        return redirect()->route('customer.index')->with('success', 'Data customer berhasil dihapus');
    }
    public function getcustomer(Request $request)
    {
        $customer = Customer::findOrFail($request->id);
        return response()->json([
            'berhasil' => true,
            'customer' => $customer,
        ]);
    }
}
