<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index(Request $request)
    {
       

        $dataList = Transaksi::with('kategori')->orderBy('tanggal','desc');

        if($request->awal && $request->akhir){
            $dataList = $dataList->whereBetween('tanggal',[$request->awal,$request->akhir]);
        }

        if($request->jenis){
            $dataList =$dataList->where('type',$request->jenis);
        }


        $dataList = $dataList->get();
        $dataTotalSaldo = $dataList->sum('total');
      
        $dt = [
            'menu' => 'Data Laporan',
            'title' => 'List Laporan',
            
        ];
       
      
        
       
        return view('laporan.index', compact('dt','dataList','dataTotalSaldo'));

    }

}
