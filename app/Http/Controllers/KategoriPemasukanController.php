<?php

namespace App\Http\Controllers;

use App\Models\Kategoritransaksi;
use Illuminate\Http\Request;

class KategoriPemasukanController extends Controller
{
    public function index()
    {
        $tipeKat = 'pemasukan';

        $dataList = Kategoritransaksi::where('tipe',$tipeKat)->orderBy('order','asc')->get();
      
        $dt = [
            'menu' => 'Data Kategori Pemasukan',
            'title' => 'List Kategori Pemasukan',
            
        ];
       
      
        
       
        return view('kategori.index', compact('dt', 'dataList','tipeKat'));
    }

    public function create()
    {
        $dt = [
            'menu' => 'Kelola Kategori Pemasukan',
            'title' => 'Tambah Kategori Pemasukan Baru',
            
            
        ];

        $tipeKat = 'pemasukan';
       
        return view('kategori.create', compact('dt','tipeKat'));
    }

    public function store(Request $request)
    {
        $rules = [
            
            'nama'     => 'required',
            'order'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $data = new Kategoritransaksi();

        $data->tipe = 'pemasukan';
        $data->nama = $request->nama;
        $data->order = $request->order;

        $data->save();

        return redirect()->route('kategori.pemasukan.index')->with('success', 'Data kategori pemasukan berhasil disimpan');
    }

    public function edit(string $id)
    {
        $data = Kategoritransaksi::find($id);

        $dt = [
            'menu' => 'Kelola Kategori Pemasukan',
            'title' => 'Edit Kategori Pemasukan ',
            
        ];

        $tipeKat = 'pemasukan';
       

        return view('kategori.edit', compact('dt','data','tipeKat'));
    }

    public function update(Request $request,$id)
    {
        $data = Kategoritransaksi::find($id);

        $rules = [
            
            'nama'     => 'required',
            'order'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

      

        $data->tipe = 'pemasukan';
        $data->nama = $request->nama;
        $data->order = $request->order;

        $data->save();

        return redirect()->route('kategori.pemasukan.index')->with('success', 'Data kategori pemasukan berhasil disimpan');
    }

    public function destroy(string $id)
    {
        $data = Kategoritransaksi::find($id);
        $data->delete();

        return redirect()->route('kategori.pemasukan.index')->with('success', 'Data kategori pemasukan berhasil dihapus');
    }

}
