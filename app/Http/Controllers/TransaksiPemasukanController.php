<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Kategoritransaksi;
use App\Models\Kota;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class TransaksiPemasukanController extends Controller
{
    public function index()
    {
        $tipeKat = 'pemasukan';

        $dataList = Transaksi::with('kategori','customer')->where('type',$tipeKat)->orderBy('tanggal','desc')->get();
      
        $dt = [
            'menu' => 'Data Transaksi Pemasukan',
            'title' => 'List Transaksi Pemasukan',
            
        ];
       
      
        
       
        return view('transaksi.index', compact('dt', 'dataList','tipeKat'));
    }

    public function create()
    {
        $dt = [
            'menu' => 'Kelola Transaksi Pemasukan',
            'title' => 'Tambah Transaksi Pemasukan Baru',
            
            
        ];

        $tipeKat = 'pemasukan';

        $kategoriList = Kategoritransaksi::where('tipe',$tipeKat)->orderBy('order','asc')->get();

        $customerList = Customer::orderBy('nama','asc')->get();

        $kotaList = Kota::all();
       
        return view('transaksi.create', compact('dt','tipeKat','kategoriList','customerList','kotaList'));
    }

    public function store(Request $request)
    {

        // dd($request->customerForm);

        $rules = [
            
            'tanggal'     => 'required',
            'kategori'      => 'required',
            'total'=> 'required',
            'customer'=> Rule::requiredIf($request->customerForm == 2),
            'nik'      => [Rule::requiredIf($request->customerForm == 1),'nullable','numeric','digits:16'],
           
            'nama' => Rule::requiredIf($request->customerForm == 1),
            'alamat' => Rule::requiredIf($request->customerForm == 1),
            'telp' => Rule::requiredIf($request->customerForm == 1),
            'kota' => Rule::requiredIf($request->customerForm == 1),

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
            'numeric' => 'Nomor NIK harus diisi dengan angka',
            'digits' => 'Nomor NIK harus  berjumlah 16 angka',
            'nik.required' => 'belum diisi',
            'kota.required' => 'belum diisi',
            'nama.required' => 'belum diisi',
            'alamat.required' => 'belum diisi',
            'telp.required' => 'belum diisi',
        ];

        $this->validate($request, $rules,$errorMessages);

        $custID = $request->customer;

        if($request->customerForm == 1 ){

            $telp = $request->telp;

            $nta = Str::is('0*', $telp);
            $ntb = Str::is('62*', $telp);

            if($nta){
                $slice = Str::after($telp,'0');
                $wa = '62'.$slice;
            }

            if($ntb){
                $wa = $telp;
            }

            $cust = new Customer();

            $cust->nik = $request->nik;
            $cust->nama = $request->nama;
            $cust->alamat = $request->alamat;
            $cust->telp = $wa;
            $cust->kota_id = $request->kota;
            $cust->user_id = 0;

            $cust->save();

            $custID = $cust->id;

        }

        $data = new Transaksi();

        $data->type = 'pemasukan';
        $data->kategori_id = $request->kategori;
        $data->tanggal = $request->tanggal;
        $data->customer_id = $custID;
        $data->keterangan = $request->keterangan;
        $data->total = $request->total;

        $data->save();

        return redirect()->route('transaksi.pemasukan.index')->with('success', 'Data kategori pemasukan berhasil disimpan');
    }

}
