<?php

namespace App\Http\Controllers;

use App\Models\Jasa;
use Illuminate\Http\Request;

class JasaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $dataList = Jasa::all();
      
        $dt = [
            'menu' => 'Data Jasa',
            'title' => 'List Jasa',
            
        ];
       
        return view('jasa.index', compact('dt', 'dataList'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $dt = [
            'menu' => 'Kelola Jasa',
            'title' => 'Tambah Jasa Baru',
            
            
        ];
       
        return view('jasa.create', compact('dt'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rules = [
            
            'jasa'     => 'required',
            'biaya'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $data = new Jasa();

        $data->jenis_jasa = $request->jasa;
        $data->biaya = $request->biaya;
      

        $data->save();

        return redirect()->route('jasa.index')->with('success', 'Data Jasa berhasil disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Jasa::find($id);

        $dt = [
            'menu' => 'Kelola Jasa',
            'title' => 'Edit Jasa ',
            
        ];
       

        return view('jasa.edit', compact('dt','data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Jasa::find($id);

        $rules = [
            
            'jasa'     => 'required',
            'biaya'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

       
        $data->jenis_jasa = $request->jasa;
        $data->biaya = $request->biaya;
      

        $data->save();

        return redirect()->route('jasa.index')->with('success', 'Data Jasa berhasil diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Jasa::find($id);
        $data->delete();

        return redirect()->route('jasa.index')->with('success', 'Data jasa berhasil dihapus');
    }
}
