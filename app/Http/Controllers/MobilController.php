<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Menu;
use App\Models\Mobil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MobilController extends Controller
{
    public function index(): View
    {

        $mobilList = Mobil::all();
      
        $dt = [
            'menu' => 'Data Kendaraan',
            'title' => 'List Kendaraan',
            'mobilList' => $mobilList,
            
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];
        return view('mobil.index', compact('dt', 'menu'));
    }

    public function create(): View
    {
      
        $dt = [
            'menu' => 'Kelola Kendaraan',
            'title' => 'Tambah Kendaraan Baru',
            
            
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];
        return view('mobil.create', compact('dt', 'menu'));
    }

    public function store(Request $request)
    {

        $rules = [
            
            'merek'     => 'required',
            'tipe'      => 'required',
            'warna'     => 'required',
            'harga_6'     => 'required',
            'harga_12'     => 'required',
            'harga_24'     => 'required',
            'harga_day'     => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $mobil = new Mobil();

        $mobil->nopol = $request->nopol;
        $mobil->merek = $request->merek;
        $mobil->tipe = $request->tipe;
        $mobil->warna = $request->warna;
        $mobil->harga_6 = $request->harga_6;
        $mobil->harga_12 = $request->harga_12;
        $mobil->harga_24 = $request->harga_24;
        $mobil->harga_day = $request->harga_day;
        $mobil->status = 1;


        $mobil->save();

        return redirect()->route('mobil.index')->with('success', 'Data kendaraan berhasil disimpan');

    }

    public function edit($id)
    {

        $mobil = Mobil::find($id);

        $dt = [
            'menu' => 'Kelola Kendaraan',
            'title' => 'Tambah Kendaraan Baru',
            
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];

        return view('mobil.edit', compact('dt', 'menu','mobil'));

    }

    public function update(Request $request, $id)
    {
        $mobil = Mobil::find($id);

        $rules = [
            
            'merek'     => 'required',
            'tipe'      => 'required',
            'warna'     => 'required',
            'harga_6'     => 'required',
            'harga_12'     => 'required',
            'harga_24'     => 'required',
            'harga_day'     => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

       

        $mobil->nopol = $request->nopol;
        $mobil->merek = $request->merek;
        $mobil->tipe = $request->tipe;
        $mobil->warna = $request->warna;
        $mobil->harga_6 = $request->harga_6;
        $mobil->harga_12 = $request->harga_12;
        $mobil->harga_24 = $request->harga_24;
        $mobil->harga_day = $request->harga_day;

        $mobil->save();

        return redirect()->route('mobil.index')->with('success', 'Data kendaraan berhasil diperbaharui');
    }

    public function destroy($id)
    {
        $mobil = Mobil::find($id);
        $mobil->delete();

        return redirect()->route('mobil.index')->with('success', 'Data kendaraan berhasil dihapus');
    }
}
