<?php

namespace App\Http\Controllers;

use App\Models\Driverfee;
use Illuminate\Http\Request;

class DriverfeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        
        $dataList = Driverfee::all();
      
        $dt = [
            'menu' => 'Data Harga Driver',
            'title' => 'List Harga Driver', 
        ];
       
        return view('driverfee.index', compact('dt','dataList'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $dt = [
            'menu' => 'Kelola Harga Driver',
            'title' => 'Tambah Harga Driver Baru',
            
            
        ];
       
        return view('driverfee.create', compact('dt'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rules = [
            
            'judul'     => 'required',
            'durasi'      => 'required',
            'harga'     => 'required',
            'order'     => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $data = new Driverfee();

        $data->judul = $request->judul;
        $data->durasi = $request->durasi;
        $data->harga = $request->harga;
        $data->order = $request->order;
        
    

        $data->save();

        return redirect()->route('driverfee.index')->with('success', 'Data harga driver berhasil disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Driverfee::find($id);

        $dt = [
            'menu' => 'Kelola Harga Driver ',
            'title' => 'Edit Harga Driver  ',
            
        ];
       

        return view('driverfee.edit', compact('dt','data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Driverfee::find($id);

        $rules = [
            
            'judul'     => 'required',
            'durasi'      => 'required',
            'harga'     => 'required',
            'order'     => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $data->judul = $request->judul;
        $data->durasi = $request->durasi;
        $data->harga = $request->harga;
        $data->order = $request->order;
        
    

        $data->save();

        return redirect()->route('driverfee.index')->with('success', 'Data harga driver berhasil diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Driverfee::find($id);
        $data->delete();

        return redirect()->route('driverfee.index')->with('success', 'Data harga driver berhasil dihapus');
    }
}
