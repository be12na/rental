<?php

namespace App\Http\Controllers;

use App\Models\Kategoritransaksi;
use Illuminate\Http\Request;

class KategoriPengeluaranController extends Controller
{
    public function index()
    {
        $tipeKat = 'pengeluaran';

        $dataList = Kategoritransaksi::where('tipe',$tipeKat)->orderBy('order','asc')->get();
      
        $dt = [
            'menu' => 'Data Kategori Pengeluaran',
            'title' => 'List Kategori Pengeluaran',
            
        ];
       
      
        
       
        return view('kategori.index', compact('dt', 'dataList','tipeKat'));
    }

    public function create()
    {
        $dt = [
            'menu' => 'Kelola Kategori Pengeluaran',
            'title' => 'Tambah Kategori Pengeluaran Baru',
            
            
        ];

        $tipeKat = 'pengeluaran';
       
        return view('kategori.create', compact('dt','tipeKat'));
    }

    public function store(Request $request)
    {
        $rules = [
            
            'nama'     => 'required',
            'order'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $data = new Kategoritransaksi();

        $data->tipe = 'pengeluaran';
        $data->nama = $request->nama;
        $data->order = $request->order;

        $data->save();

        return redirect()->route('kategori.pengeluaran.index')->with('success', 'Data kategori pengeluaran berhasil disimpan');
    }

    public function edit(string $id)
    {
        $data = Kategoritransaksi::find($id);

        $dt = [
            'menu' => 'Kelola Kategori Pengeluaran',
            'title' => 'Edit Kategori Pengeluaran ',
            
        ];

        $tipeKat = 'pengeluaran';
       

        return view('kategori.edit', compact('dt','data','tipeKat'));
    }

    public function update(Request $request,$id)
    {
        $data = Kategoritransaksi::find($id);

        $rules = [
            
            'nama'     => 'required',
            'order'      => 'required',
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

      

        $data->tipe = 'pengeluaran';
        $data->nama = $request->nama;
        $data->order = $request->order;

        $data->save();

        return redirect()->route('kategori.pengeluaran.index')->with('success', 'Data kategori pengeluaran berhasil disimpan');
    }

    public function destroy(string $id)
    {
        $data = Kategoritransaksi::find($id);
        $data->delete();

        return redirect()->route('kategori.pengeluaran.index')->with('success', 'Data kategori pengeluaran berhasil dihapus');
    }
}
