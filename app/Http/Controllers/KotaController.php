<?php

namespace App\Http\Controllers;

use App\Models\Kota;
use App\Models\Menu;
use Illuminate\Http\Request;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kotaList = Kota::all();
      
        $dt = [
            'menu' => 'Data Kota',
            'title' => 'List Kota',
          
            
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];

        return view('kota.index', compact('dt', 'menu','kotaList'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $dt = [
            'menu' => 'Kelola Kota',
            'title' => 'Tambah Kota Baru',
            
            
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];
        return view('kota.create',compact('dt','menu'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rules = [
            
            'kota'     => 'required',
           
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $kota = new Kota();

        $kota->kota = $request->kota;
  
        $kota->save();

        return redirect()->route('kota.index')->with('success', 'Data Kota berhasil disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $kota = Kota::find($id);

        $dt = [
            'menu' => 'Kelola Kota',
            'title' => 'Edit Kota',
            
            
        ];
        $menu = [
            'menu' => Menu::all()->sortBy('urutan'),
        ];
        return view('kota.edit',compact('dt','menu','kota'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $kota = Kota::find($id);

        $rules = [
            
            'kota'     => 'required',
           
            
            

		];

		$errorMessages = [
            'required' => 'Kolom harus diisi',
        ];

        $this->validate($request, $rules, $errorMessages);

        $kota->kota = $request->kota;
  
        $kota->save();

        return redirect()->route('kota.index')->with('success', 'Data Kota berhasil diperbaharuai');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $kota = Kota::find($id);
        $kota->delete();

        return redirect()->route('kota.index')->with('success', 'Data kota berhasil dihapus');
    }
}
