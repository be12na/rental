<?php

namespace App\View\Components\Stats;

use App\Models\Customer as ModelsCustomer;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Customer extends Component
{
    /**
     * Create a new component instance.
     */
    public $total;
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $this->total = ModelsCustomer::count();
        return view('components.stats.customer');
    }
}
