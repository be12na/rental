<?php

namespace App\View\Components\Stats;

use App\Models\Transaksi;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Saldo extends Component
{
    /**
     * Create a new component instance.
     */
    public $total;
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $masuk = Transaksi::where('type','pemasukan')->sum('total');
        $keluar = Transaksi::where('type','pengeluaran')->sum('total');

        $this->total = $masuk-$keluar;


        return view('components.stats.saldo');
    }
}
