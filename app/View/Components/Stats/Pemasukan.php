<?php

namespace App\View\Components\Stats;

use App\Models\Transaksi;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Pemasukan extends Component
{
    /**
     * Create a new component instance.
     */
    public $total;
    public function __construct()
    {
        $this->total;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $this->total = Transaksi::where('type','pemasukan')->sum('total');

        return view('components.stats.pemasukan');
    }
}
